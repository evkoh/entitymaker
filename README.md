# EntityMaker

## Setup fronts 

```
docker-compose --file docker-compose.yml --file docker-compose.override-network.yml up -d
```

OR if you are on native Linux (not in virtual machine) :

```
docker-compose --file docker-compose.yml --file docker-compose.override-network-host.yml up -d
```

## Install dependancies

```
docker exec -it entityMaker yarn install
```

## Serve fronts

AND serve your project with NodeJS :

```
docker exec -it entityMaker yarn run serve
```

## Show fronts

Frontoffice : Go on http://127.0.0.1 ou http://127.0.0.1:4200

# todo yarn add js-yaml htmlhint