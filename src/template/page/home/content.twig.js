export let template = function (vars) {

    return `
    <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
    <link rel="stylesheet" href="https://codemirror.net/theme/monokai.css">
    <link rel="stylesheet" href="https://codemirror.net/addon/lint/lint.css">
<!--    -->
<!--    <script src="https://unpkg.com/jshint@2.9.6/dist/jshint.js"></script>-->
<!--    <script src="https://unpkg.com/jsonlint@1.6.3/web/jsonlint.js"></script>-->
<!--    <script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>-->

<!--<script src="https://codemirror.net/addon/lint/lint.js"></script>-->
<!--<script src="https://codemirror.net/addon/lint/javascript-lint.js"></script>-->
<!--<script src="https://codemirror.net/addon/lint/json-lint.js"></script>-->
<!--<script src="https://codemirror.net/addon/lint/css-lint.js"></script>-->
    
    <div class="panels left">
        <textarea id="editorYaml" name="editorYaml"></textarea>
                <div class="box">
          <ul class="directory-list"></ul>
        </div>
    </div>
    <div class="panels right notAllowed">
        <div id="fileHeader">
            <span id="phpFileInfoName" class="info">Please select a file</span> | 
            <span id="phpFileInfoPath" class="info">Wait file selected</span>
        </div>
        <textarea id="editorPhp" name="editorPhp"></textarea>
    </div>
    <div id="bottom">
        <div class="tabbed">
            <input type="radio" name="tabs" id="tab-nav-1"><label for="tab-nav-1">Dashboard</label>
            <input type="radio" name="tabs" id="tab-nav-2" checked><label for="tab-nav-2">Logs</label>
            <input type="radio" name="tabs" id="tab-nav-3"><label for="tab-nav-3">About</label>
            <input type="radio" name="tabs" id="tab-nav-4"><label for="tab-nav-4">Features / Issues</label>
            <input type="radio" name="tabs" id="tab-nav-5"><label for="tab-nav-5">Help (French)</label>
            <div class="tabs">
                <div id="dashboard">
                    <button id="createBackup">Create backup</button> 
                    <button id="createZip">Download Zip</button>
                    <button id="resetAll">Reset All Files</button>
                </div>
                <div id="logs"></div>
                <div id="about">
                    Version : 0.1<br />
                    Power By : <a href="https://gitlab.com/evkoh" target="_blank">EvKoh</a><br />
                    Fork gitlab Project : <a href="https://gitlab.com/evkoh/entitymaker" target="_blank">https://gitlab.com/evkoh/entitymaker</a>
                </div>
                <div id="feature">
                    <a href="https://gitlab.com/evkoh/entitymaker/-/issues" target="_blank">https://gitlab.com/evkoh/entitymaker/-/issues</a><br />
                    Add issue, send email to : <a href="mailto:incoming+evkoh-entitymaker-24244116-9bj4c8y6nrt3aycamu0bf5kll-issue@incoming.gitlab.com">incoming+evkoh-entitymaker-24244116-9bj4c8y6nrt3aycamu0bf5kll-issue@incoming.gitlab.com</a>
                </div>
                <div id="aide">
                    Aide:<br />
                    <br />
                    ...
                </div>
            </div>
        </div>
    </div>
    `;
}