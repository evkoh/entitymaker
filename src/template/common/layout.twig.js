export let template = function (vars) {
    return `
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Accueil | EntityMaker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
{{header}}
{{content}}
{{footer}}
</body>
</html>
`;
}