import Router from "minimal-router";

import ("../scss/app/index.scss");

/**
 *e
 */
window.dump = function () {
    let datas = {};
    datas.trace = [];

    let trace = new Error().stack.split('\n');
    trace.forEach(function (line, key) {
        if (0 !== key) {
            datas.trace.push(line.trim());
        }
    });

    let args = Array.prototype.slice.call(arguments);
    args.forEach(function (element, key) {
        datas[key] = element;
    }, this);

    if (typeof window.traces === 'undefined') {
        window.traces = [];
    }

    window.traces.push(datas);

    console.debug(datas);
}

/**
 *
 */
function configureRouter() {
    const router = new Router();

    router.add('home', '/', function () {
        require("../js/home").run();
    });

    return router;
}

/**
 *
 * @param router
 */
function dispatchRouter(router) {
    let path = document.location.pathname;

    if (document.location.search.length) {
        path += '?' + document.location.search;
    }

    if (document.location.search.hash) {
        path += '#' + document.location.hash;
    }

    router.dispatch(path);
}

dispatchRouter(configureRouter());