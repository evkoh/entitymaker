import {codeMirrorMode, isDirExist} from "../app/plugins/utils/fileManager";

let template = require('../app/plugins/utils/template');
let engine = require('../app/plugins/engine/index');
let divers = require('../app/plugins/utils/divers');
let notify = require('../app/plugins/notify/index');

require("codemirror/mode/javascript/javascript");
require("codemirror/addon/edit/closebrackets");
let CodeMirror = require("codemirror");

import ("../../scss/home/index.scss");
import ("../app/plugins/fileList/index");
import ("../app/plugins/fileList/index.scss");
let fileManager = require('../app/plugins/utils/fileManager');

import 'codemirror/lib/codemirror.js';

// CodeMirror Mode
import 'codemirror/mode/yaml/yaml';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/css/css';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/php/php';

// CodeMirror Lint
import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/addon/search/search';
import 'codemirror/addon/selection/active-line';

window.jsyaml = require('js-yaml');
import 'codemirror/addon/lint/lint';
import 'codemirror/addon/lint/yaml-lint';
// import '../app/plugins/utils/php-lint';
// import 'codemirror/addon/lint/css-lint';
// import 'codemirror/addon/lint/html-lint';

// CodeMirror Hint
// import 'codemirror/addon/hint/html-hint';
// import 'codemirror/addon/hint/css-hint';
// import 'codemirror/addon/hint/javascript-hint';

fileManager.addDir('/', 'global');
fileManager.addDir('/', 'backups');
fileManager.addDir('/', 'workspace');
if (!fileManager.isFileExist('/global/contract.yaml')) {
    fileManager.addFile('/global/contract.yaml', require('../app/plugins/engine/example').default, function () {
        notify.success('Setup contract.yaml with example !', 'bottom left', 3000, false);
    });
}

export function run() {
    template.twigRenderDom(require('../../template/common/layout.twig'), {
        header: template.twigRender(require('../../template/common/header.twig'), {}),
        content: template.twigRender(require('../../template/page/home/content.twig'), {}),
        footer: template.twigRender(require('../../template/common/footer.twig'), {}),
    });

    $(document).ready(function () {
        window.editorYaml = CodeMirror.fromTextArea(document.querySelector('#editorYaml'), {
            lineNumbers: true,
            tabSize: 4,
            theme: 'monokai',
            mode: fileManager.codeMirrorMode('yaml'),
            gutters: ["CodeMirror-lint-markers"],
            lint: true
        });

        window.editorPhp = CodeMirror.fromTextArea(document.querySelector('#editorPhp'), {
            lineNumbers: true,
            matchBrackets: true,
            mode: fileManager.codeMirrorMode('php'),
            theme: 'monokai',
            indentUnit: 4,
            indentWithTabs: true,
            readOnly: true,
            gutters: ["CodeMirror-lint-markers"],
            lint: true
        })

        window.editorYaml.on('change', divers.delay(function () {
            engine.parseYaml(window.editorYaml.getValue());

            // Update contract.yaml
            fileManager.updateFile('/global/contract.yaml', function () {
                return window.editorYaml.getValue();
            }, function (file) {
                fileManager.openLastFile();
            });

            notify.success('contract.yaml is now updated !');
        }, 500));

        fileManager.fileListGenerator();
        fileManager.openContractFile('/global/contract.yaml');

        $('#createBackup').click(function () {
            fileManager.createBackup();
        });

        $('#createZip').click(function () {
            fileManager.createZip();
        });

        $('#resetAll').click(function () {
            if (confirm("Do you want to delete everything ?")) {
                localStorage.removeItem('dir::/');
                document.location.reload();
                alert('Everything has been deleted !');
            } else {
                alert('Nothing has been deleted :)');
            }
        });
    });
}