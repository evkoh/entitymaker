require('./notify.min');

/**
 *
 * @param message
 * @param position
 * @param duration
 * @param clearBefore
 */
export let error = function (message, position = 'bottom right', duration = 2000, clearBefore = true) {
    if (true === clearBefore) {
        clear();
    }

    $.notify(message, {
        className: 'error',
        position: position,
        showDuration: 50,
        hideDuration: 50,
        autoHideDelay: duration
    });
}

/**
 *
 * @param message
 * @param position
 * @param duration
 * @param clearBefore
 */
export let success = function (message, position = 'bottom right', duration = 2000, clearBefore = true) {
    if (true === clearBefore) {
        clear();
    }

    $.notify(message, {
        className: 'success',
        position: position,
        showDuration: 50,
        hideDuration: 50,
        autoHideDelay: duration
    });
}

/**
 *
 * @param message
 * @param position
 * @param duration
 * @param clearBefore
 */
export let info = function (message, position = 'bottom right', duration = 2000, clearBefore = true) {
    if (true === clearBefore) {
        clear();
    }

    $.notify(message, {
        className: 'info',
        position: position,
        showDuration: 50,
        hideDuration: 50,
        autoHideDelay: duration
    });
}

/**
 *
 */
export let clear = function () {
    //$('.notifyjs-corner').html('');
}