const moment = require('moment');

/**
 *
 * @param code
 * @param message
 */
export let error = function (code, message) {
    if (typeof window.logsStack === 'undefined') {
        window.logsStack = [];
    }

    window.logsStack.push({
        type: 'error',
        message: moment().format('YYYY-MM-DD HH:mm:ss') + ' Error : [' + code + '] ' + message
    });

    displayLogs();
}

/**
 *
 */
export let clearLogs = function () {
    if (typeof window.logsStack === 'undefined') {
        window.logsStack = [];
    }
    window.logsStack = [];
    displayLogs();
}

/**
 *
 */
export let displayLogs = function () {
    if (typeof window.logsStack === 'undefined') {
        window.logsStack = [];
    }

    let html = '';

    window.logsStack.forEach(function (log, key) {
        html += '<span type="' + log.type + '">' + log.message + '</span><br />';
    });

    $('#logs').html(html);
    openLogsTabs();
}

/**
 *
 * @param force
 */
export let openLogsTabs = function (force = false) {
    if (typeof window.logsStack === 'undefined') {
        window.logsStack = [];
    }

    if (window.logsStack.length > 0 || true === force) {
        $('[for="tab-nav-2"]').click();
    }
}