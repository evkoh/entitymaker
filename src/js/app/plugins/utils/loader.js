// /**
//  *
//  * @param callbackReady
//  */
// export let ready = function (callbackReady) {
//     if (typeof window.isReady === 'undefined') {
//         window.isReady = false;
//     }
//
//     if (true === window.isReady) {
//         callbackReady();
//     } else {
//         let timer = setInterval(function () {
//             if (true === window.isReady) {
//                 clearInterval(timer);
//                 callbackReady();
//             }
//         }, 100)
//     }
// }
//
// /**
//  *
//  * @param isReady
//  */
// export let setReady = function (isReady) {
//     window.isReady = isReady;
// }
//
// $(document).ready(function () {
//     //setTimeout(function () {
//         window.isReady = true;
//     //}, 2000);
// });

/**
 *
 * @param element
 * @param callbackReady
 */
export let waitElementExist = function (element, callbackReady) {
    let timer = setInterval(function () {
        if (typeof callbackReady !== 'undefined' && typeof element !== 'undefined') {
            clearInterval(timer);
            callbackReady();
        }
    }, 100)
}

/**
 *
 * @param elements
 * @param callbackReady
 * @param index
 */
export let waitElementsExist = function (elements, callbackReady, index) {
    if (typeof index === 'undefined') {
        index = 0;
    }

    if (typeof elements[index] !== 'undefined') {
        waitElementExist(elements[index], function () {
            waitElementsExist(elements, callbackReady, index + 1);
        })
    } else {
        if (typeof callbackReady !== 'undefined') {
            callbackReady();
        }
    }
}

/**
 *
 * @param element
 * @param callbackReady
 */
export let waitElementIsNotNull = function (element, callbackReady) {
    let timer = setInterval(function () {
        if (typeof callbackReady !== 'undefined' && element !== null) {
            clearInterval(timer);
            callbackReady();
        }
    }, 100)
}

/**
 *
 * @param elements
 * @param callbackReady
 * @param index
 */
export let waitElementsIsNotNull = function (elements, callbackReady, index) {
    if (typeof index === 'undefined') {
        index = 0;
    }

    if (typeof elements[index] !== 'undefined') {
        waitElementIsNotNull(elements[index], function () {
            waitElementsIsNotNull(elements, callbackReady, index + 1);
        })
    } else {
        if (typeof callbackReady !== 'undefined') {
            callbackReady();
        }
    }
}

/**
 *
 * @param selector
 * @param callbackReady
 */
export let waitDomElementExist = function (selector, callbackReady) {
    let timer = setInterval(function () {
        if (typeof callbackReady !== 'undefined' && $(selector).length !== 0) {
            clearInterval(timer);
            callbackReady();
        }
    }, 100)
}

/**
 *
 * @param eventName
 * @param callbackReady
 */
export let waitEventPop = function (eventName, callbackReady) {
    let timer = setInterval(function () {
        if (typeof callbackReady !== 'undefined' && typeof window[eventName] !== 'undefined' && window[eventName] === true) {
            window[eventName] = 'ready';
            clearInterval(timer);
            callbackReady();
        }
    }, 100)
}