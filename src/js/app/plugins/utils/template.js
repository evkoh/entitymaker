let Mustache = require("mustache");
let twig = require('jstransformer')(require('jstransformer-twig'));
let loader = require('./loader');

/**
 *
 * @param file
 * @param vars
 * @returns {*}
 */
export let mustacheRender = function (file, vars) {
    return Mustache.render(file.template(), vars);
}

/**
 *
 * @param string
 * @param vars
 * @returns {*}
 */
export let mustacheRenderFromString = function (string, vars) {
    return Mustache.render(string, vars);
}


/**
 *
 * @param templateName
 * @param file
 * @param vars
 */
export let mustacheRenderElement = function (templateName, file, vars) {
    loader.ready(function () {
        $('[templateName="' + templateName + '"]').html(mustacheRender(file, vars));
    });
}

/**
 *
 * @param file
 * @param vars
 * @returns {*}
 */
export let twigRenderQuery = function (file, vars) {
    return twig.render(file.query, vars).body;
}

/**
 *
 * @param file
 * @param vars
 * @returns {*}
 */
export let twigRender = function (file, vars) {
    return twig.render(file.template(vars), vars).body;
}

/**
 *
 * @param templateName
 * @param file
 * @param vars
 */
export let twigRenderElement = function (templateName, file, vars) {
    loader.ready(function () {
        $('[templateName="' + templateName + '"]').html(twigRender(file, vars));
    });
}

/**
 *
 * @param file
 * @param vars
 */
export let twigRenderDom = function (file, vars) {
    let newDoc = document.open("text/html", "replace");
    newDoc.write(twigRender(file, vars));
    newDoc.close();
}