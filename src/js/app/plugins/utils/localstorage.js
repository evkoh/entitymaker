/**
 *
 * @param key
 * @returns {boolean}
 */
export let has = function (key) {
    return null !== localStorage.getItem(key);
}

/**
 *
 * @param key
 * @param object
 * @returns {*}
 */
export let setObject = function (key, object) {
    localStorage.setItem(key, JSON.stringify(object));

    return getObject(key);
}

/**
 *
 * @param key
 * @returns {any}
 */
export let getObject = function (key) {
    return JSON.parse(localStorage.getItem(key));
}

/**
 *
 * @param key
 * @param callbackUpdate
 */
export let updateObject = function (key, callbackUpdate) {
    setObject(key, callbackUpdate(getObject(key)));
}