let localStorageManager = require('./localstorage');
window.jsyaml = require('js-yaml');
const moment = require('moment');
let fileList = require('../fileList/index');
let notify = require('../notify/index');
let JSZip = require("jszip");
import {saveAs} from 'file-saver';

/**
 *
 * @param name
 * @param childs
 * @returns {{path: string, name: *, type: string, childs: *[][]}}
 */
let createDirObject = function (name, childs) {
    let path = '/' + name;

    if (typeof childs === 'undefined') {
        childs = [];
    }

    if (!Array.isArray(childs)) {
        childs = [childs];
    }

    childs = addCurrentPathToChilds(path, childs);

    return {
        name: name,
        path: path,
        isLocked: false,
        type: 'dir',
        childs: childs
    }
}

/**
 *
 * @param childs
 * @returns {*}
 */
let createBaseDir = function (childs) {
    let key = 'dir::/';

    if (!localStorageManager.has(key)) {
        localStorageManager.setObject(key, createDirObject('', childs));
    }

    return localStorageManager.getObject(key);
}

/**
 *
 * @returns {*}
 */
export let getBaseDir = function () {
    let key = 'dir::/';

    return localStorageManager.getObject(key);
}

/**
 *
 * @param search
 * @param toAdd
 * @param parent
 * @param callBackSuccess
 * @returns {*}
 */
let addChildToDir = function (search, toAdd, parent, callBackSuccess) {
    if (typeof search !== 'string') {
        console.error('Le 1er argument doit etre de type string !');
    }

    if (typeof parent === 'undefined') {
        parent = addChildToDir(search, toAdd, getBaseDir(), callBackSuccess);
        localStorageManager.setObject('dir::/', parent);
        return parent;
    }

    if (parent.path === search) {

        if ('/' !== parent.path) {
            toAdd.path = parent.path + toAdd.path;
        }

        let alreadyExist = false;
        parent.childs.forEach(function (valueChild, keyChild) {
            if (valueChild.name === toAdd.name) {
                alreadyExist = true;
            }
        })

        if (false === alreadyExist) {
            parent.childs.push(toAdd);

            if (typeof callBackSuccess !== 'undefined') {
                callBackSuccess(toAdd);
            }
        } else {
            if (toAdd.type === 'dir') {
                console.warn('Le répertoire ' + search + '/' + toAdd.name + ' existe déjà !');
            } else if (toAdd.type === 'file') {
                notify.error('Le fichier ' + search + '/' + toAdd.name + ' existe déjà !');
            }
        }

        return parent;
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            parent.childs[keyChild] = addChildToDir(search, toAdd, valueChild, callBackSuccess);
        })
    }

    return parent;
}

/**
 *
 * @param search
 * @param parent
 * @returns {*}
 */
export let remove = function (search, parent) {
    if (typeof search !== 'string') {
        console.error('Le 1er argument doit etre de type string !');
    }

    if (typeof parent === 'undefined') {
        parent = remove(search, getBaseDir());
        localStorageManager.setObject('dir::/', parent);
        return parent;
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            if (parent.childs[keyChild].path === search) {
                parent.childs.splice(keyChild, 1);
                return parent;
            }

            parent.childs[keyChild] = remove(search, valueChild);
        })
    }

    fileListGenerator();

    return parent;
}

/**
 *
 * @param search
 * @param callback
 * @param callbackSuccess
 * @param parent
 * @returns {*}
 */
export let updateFile = function (search, callback, callbackSuccess, parent) {
    if (typeof search !== 'string') {
        console.error('Le 1er argument doit etre de type string !');
    }

    if (typeof parent === 'undefined') {
        parent = updateFile(search, callback, callbackSuccess, getBaseDir());
        localStorageManager.setObject('dir::/', parent);
        return parent;
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            if (parent.childs[keyChild].path === search && parent.childs[keyChild].type === 'file') {
                parent.childs[keyChild].content = callback(parent.childs[keyChild].content);
                callbackSuccess(parent.childs[keyChild]);
                return parent;
            }

            parent.childs[keyChild] = updateFile(search, callback, callbackSuccess, valueChild);
        })
    }

    fileListGenerator();

    return parent;
}

/**
 *
 * @param search
 * @param callback
 * @param parent
 * @returns {*}
 */
export let getDir = function (search, callback, parent) {
    if (typeof search !== 'string') {
        console.error('Le 1er argument doit etre de type string !');
    }

    if (typeof parent === 'undefined') {
        return getDir(search, callback, getBaseDir());
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            if (parent.childs[keyChild].path === search) {
                callback(parent.childs[keyChild]);
                return parent;
            }

            parent.childs[keyChild] = getDir(search, callback, valueChild);
        })
    }

    return parent;
}

/**
 *
 * @param search
 * @param callback
 * @param parent
 * @returns {*}
 */
export let getFile = function (search, callback, parent) {
    if (typeof search !== 'string') {
        console.error('Le 1er argument doit etre de type string !');
    }

    if (typeof parent === 'undefined') {
        return getFile(search, callback, getBaseDir());
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            if (parent.childs[keyChild].path === search) {
                callback(parent.childs[keyChild]);
                return parent;
            }

            parent.childs[keyChild] = getFile(search, callback, valueChild);
        })
    }

    return parent;
}

/**
 *
 * @param search
 * @returns {boolean}
 */
export let isFileExist = function (search) {
    let isExist = false;

    getFile(search, function () {
        isExist = true;
    });

    return isExist;
}

/**
 *
 * @param search
 * @returns {boolean}
 */
export let isDirExist = function (search) {
    let isExist = false;

    getDir(search, function () {
        isExist = true;
    });

    return isExist;
}

/**
 *
 * @param path
 * @param name
 */
export let addDir = function (path, name) {
    console.log('Create DIR : ' + name + ' in ' + path);
    createBaseDir();

    if (!isDirExist(path + name)) {
        addChildToDir(path, createDirObject(name));
        fileListGenerator();
    }
}

/**
 *
 * @param path
 */
export let addDirRecursive = function (path) {
    let dirs = path.split('/');
    delete dirs[0];
    let createdDirs = [];

    for (let i = 1; i < dirs.length; i++) {

        let inPath;
        if (createdDirs.length === 0) {
            inPath = '/';
        } else {
            inPath = '/' + createdDirs.join('/');
        }

        createdDirs.push(dirs[i]);
        addDir(inPath, dirs[i]);
    }
}

/**
 *
 * @param path
 * @param content
 * @param callBackSuccess
 */
export let addFile = function (path, content, callBackSuccess) {
    let array = path.split('/');
    let name = array.pop();
    path = array.join('/');

    if ('' === name) {
        path = '/';
    }

    addChildToDir(path, createFileObject(name, content), undefined, callBackSuccess);

    fileListGenerator();
}

/**
 *
 * @param content
 */
export let preparePhpFile = function (content) {

    // Add signature
    if (typeof parsed.settings.signature !== 'undefined') {
        let signature = '/**\n * ' + parsed.settings.signature + '\n */\n\n';
        content = content.replace('<?php\n\n', '<?php\n\n' + signature);
    }

    return content;
}

// /**
//  *
//  * @param path
//  * @returns {*}
//  */
// export let getDirByPath = function (path) {
//     let key = 'dir::' + path;
//     return localStorageManager.getObject(key);
// }

/**
 *
 * @param path
 * @param childs
 * @returns {*}
 */
function addCurrentPathToChilds(path, childs) {
    childs.forEach(function (childValue, childKey) {
        if ('/' !== path) {
            childs[childKey].path = path + '/' + childValue.name;
        }

        if (typeof childValue.childs !== 'undefined') {
            childValue.childs = addCurrentPathToChilds(childs[childKey].path, childValue.childs);
        }
    });

    return childs;
}

/**
 *
 * @param name
 * @param content
 * @returns {{path: string, parentDir: *, name: *, type: string, content: *}}
 */
export let createFileObject = function (name, content) {
    let path = '/' + name;

    return {
        name: name,
        path: path,
        extension: getExtension(name),
        isLocked: false,
        type: 'file',
        content: content
    }
}

/**
 *
 * @param filename
 * @returns {null|*}
 */
export let getExtension = function (filename) {
    let re = /(?:\.([^.]+))?$/;
    let extension = re.exec(filename)[1];

    if (typeof extension === 'undefined') {
        return null;
    }

    return extension;
}

/**
 *
 * @param element
 * @returns {string}
 */
export let fileListGeneratorElement = function (element) {
    let html = '';

    if (typeof element.childs !== 'undefined') {
        element.childs.forEach(function (child) {
            let fileClass = '';

            if (child.type === 'file') {
                fileClass += 'extension-' + child.extension;
            }

            html += `
           <li class="type-` + child.type + ` ` + fileClass + `" title="` + child.path + `" path="` + child.path + `">` + child.name + `
              <ul>
                ` + fileListGeneratorElement(child) + `
              </ul>
            </li>
        `;
        });
    }

    return html;
}

/**
 *
 * @returns {string}
 */
export let fileListGenerator = function () {
    let list = getBaseDir(),
        html = '';

    list.childs.forEach(function (child) {
        html += `
           <li class="type-` + child.type + `" title="` + child.path + `" path="` + child.path + `">` + child.name + `
              <ul>
                ` + fileListGeneratorElement(child) + `
              </ul>
            </li>
        `;
    });

    $('.directory-list').html(html);
    fileList.initFileList();
}

/**
 *
 * @param path
 */
export let openFileByPath = function (path) {
    getFile(path, function (file) {
        openFileByObject(file);
    });
}

/**
 *
 * @param file
 */
export let openFileByObject = function (file) {
    window.editorPhp.getDoc().setValue('');
    // window.editorPhp.setValue('');
    // window.editorPhp.clearHistory(); // todo : clearEditor(window.editorPhp) pour regrouper ces 3 fonctions !
    window.editorPhp.getDoc().setValue(file.content);
    $('#phpFileInfoName').html(file.name);
    $('#phpFileInfoPath').html(file.path);
    localStorageManager.setObject('lastOpenedFile', file);
    notify.info("Open file : " + file.path);
    window.editorPhp.setOption("mode", codeMirrorMode(file.extension));
}

/**
 *
 */
export let openLastFile = function () {
    let lastFile = localStorageManager.getObject('lastOpenedFile');
    if (typeof lastFile !== 'undefined' && null !== lastFile && isFileExist(lastFile.path)) {
        openFileByPath(lastFile.path);
    }
}

/**
 *
 * @param path
 */
export let openContractFile = function (path) {
    getFile(path, function (file) {
        window.editorYaml.getDoc().setValue(file.content);
        $('#phpFileInfoName').html(file.name);
        $('#phpFileInfoPath').html(file.path);
    });
}

/**
 *
 */
export let createBackup = function () {
    let fileName = 'contract_' + moment().format('YYYY-MM-DD-HH:mm:ss') + '.yaml';
    let filePath = '/backups/' + fileName;

    //let currentValue = file.content; // Si on souhaite la dernière sauvegarde
    let currentValue = window.editorYaml.getDoc().getValue(); // Si on souhaite la valeur en live

    getFile('/global/contract.yaml', function (file) {
        addFile(filePath, currentValue, function () {
            notify.success("Backup created : " + filePath);
        });
    });
}

/**
 *
 * @param extension
 */
export let codeMirrorMode = function (extension) {
    switch (extension) {
        case 'yaml':
            return 'yaml';
        case 'php':
            return 'application/x-httpd-php';
    }
}

/**
 *
 * @param parent
 * @returns {*}
 */
export let prepareFileForZip = function (parent) {
    if (typeof parent === 'undefined') {
        return prepareFileForZip(getBaseDir());
    }

    if (typeof parent.childs !== 'undefined') {
        parent.childs.forEach(function (valueChild, keyChild) {
            window.prepareFilesForZip.push(parent.childs[keyChild]);
            parent.childs[keyChild] = prepareFileForZip(valueChild);
        })
    }

    return parent;
}

/**
 *
 */
export let createZip = function () {
    let zip = new JSZip();
    window.prepareFilesForZip = [];
    prepareFileForZip();

    window.prepareFilesForZip.forEach(function (file) {
        if ('file' === file.type) {
            zip.file(file.path, file.content);
        }
    });

    zip.generateAsync({type: "blob"})
        .then(function (content) {
            saveAs(content, "example.zip");
        });
}

/**
 *
 * @param filePath
 * @returns {*}
 */
export let fileGetContent = function (filePath) {
    return require('raw-loader!../../../../../' + filePath).default;
}

/**
 *
 * @param filePath
 * @param callback
 */
export let addFileFromFileGetContent = function (filePath, callback) {
    let content = fileGetContent('src/php' + filePath);
    addFile('/workspace/src' + filePath, callback(content));
}