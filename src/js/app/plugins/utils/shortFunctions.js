/**
 *
 * @param data
 * @param key
 * @returns {boolean}
 */
export let inObject = function (data, key) {
    return typeof data !== 'undefined' && typeof data[key] !== 'undefined';
}

/**
 * CheckValue
 *
 * @param data
 * @param key
 * @param expectedValue
 * @returns {null|*}
 */
export let inObjectWithValue = function (data, key, expectedValue) {
    return typeof data !== 'undefined' && typeof data[key] !== 'undefined' && data[key] === expectedValue;
}

/**
 * Check
 *
 * @param data
 * @param key
 * @param defaultValue
 * @returns {null|*}
 */
export let inObjectDefaultValue = function (data, key, defaultValue = null) {
    if (inObject(data, key)) {
        return data[key];
    }

    return defaultValue;
}

/**
 *
 * @param data
 * @param value
 * @returns {boolean}
 */
export let inArray = function (data, value) {
    return typeof data !== 'undefined' && data.includes(value);
}

/**
 *
 * @param data
 * @param key
 * @param expectedValue
 * @returns {boolean}
 */
export let inArrayWithValue = function (data, key, expectedValue) {
    return typeof data !== 'undefined' && data.includes(key) && data[key] === expectedValue;
}

/**
 *
 * @param data
 * @param key
 * @param defaultValue
 * @returns {null|*}
 */
export let inArrayDefaultValue = function (data, key, defaultValue = null) {
    if (inArray(data, key)) {
        return data[key];
    }

    return defaultValue;
}

/**
 *
 * @param array
 * @returns {[]}
 */
export let uniformizeArray = function (array) {
    let assertsList = [];
    if (typeof array !== 'undefined') {
        array.forEach(function (value, key) {
            if (typeof value === 'object') {
                assertsList[key] = value;
                assertsList[key]['metadata'] = {
                    'name': Object.keys(value)[0]
                };
            } else {
                assertsList[key] = {};
                assertsList[key][value] = true;
                assertsList[key]['metadata'] = {
                    'name': value
                };
            }
        });
    }
    return assertsList;
}

/**
 *
 * @param asserts
 * @param expectedAssertName
 */
export let hasAssert = function (asserts, expectedAssertName) {
    let has = false;

    if (typeof asserts !== 'undefined') {
        asserts.forEach(function (assert) {
            if (assert.metadata.name === expectedAssertName) {
                has = true
            }
        });
    }

    return has;
}

/**
 *
 * @param asserts
 * @param wantedAssertName
 * @param callback
 */
export let getAssert = function (asserts, wantedAssertName, callback) {
    if (typeof asserts !== 'undefined') {
        if (hasAssert(asserts, wantedAssertName)) {
            asserts.forEach(function (assert) {
                if (assert.metadata.name === wantedAssertName) {
                    callback(assert);
                }
            });
        }
    }
}