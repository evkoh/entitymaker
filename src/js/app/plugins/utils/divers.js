/**
 *
 * @param string
 * @returns {string}
 */
export let capitalizeFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param string
 * @returns {string}
 */
export let lowerFirstLetter = function (string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

/**
 *
 * @param value
 * @returns {string | number | boolean}
 */
export let getTypeByValue = function (value) {
    let type = 'string';

    if (Array.isArray(value)) {
        type = 'array';
    } else if (typeof value === 'object') {
        type = 'json';
    } else if (typeof value === 'boolean') {
        type = 'boolean';
    } else if (typeof value === 'number') {
        type = 'integer';
    } else if (value.includes('::class')) {
        type = 'class';
    } else if (value.includes('@ORM')) {
        type = 'annotation';
    }

    return type;
}


/**
 *
 * @param value
 * @param preferSampleQuote
 * @param forcedType
 * @returns {string}
 */
export let displayValueForType = function (value, preferSampleQuote = true, forcedType = null) {
    let type;
    if (null === forcedType) {
        type = getTypeByValue(value);
    } else {
        type = forcedType;
    }

    let displayed;

    if (type === 'string') {
        if (true === preferSampleQuote) {
            displayed = "'" + value + "'";
        } else {
            displayed = '"' + value + '"';
        }
    } else if (type === 'boolean') {
        if (true === value) {
            displayed = 'true';
        } else {
            displayed = 'false';
        }
    } else if (type === 'integer') {
        displayed = value;
    } else if (type === 'json') {
        displayed = value;
    } else if (type === 'class') {
        displayed = value;
    } else if (type === 'annotation') {
        displayed = value;
    } else if (type === 'array') {
        displayed = '{';

        value.forEach(function (subValue) {
            displayed += displayValueForType(subValue, preferSampleQuote);
        });

        displayed += '}';
    }

    return displayed;
}

/**
 *
 * @param number
 * @returns {string}
 */
export let tab = function (number = 1) {
    let tabs = '';

    for (let i = 0; i < number; i++) {
        tabs += '    ';
    }

    return tabs;
};

/**
 *
 * @param callback
 * @param ms
 * @returns {function(): void}
 */
export let delay = function (callback, ms = 300) {
    let timer = 0;
    return function () {
        let context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}