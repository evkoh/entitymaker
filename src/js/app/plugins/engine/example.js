export default `settings:
  annotations:
    #var: true
    #ApiResource: true
    #ApiProperty: true
    #ApiSubresource: true
    #ORM\\Entity: true
    #ORM\\Table: true
    #ORM\\Column: true
    #ORM\\JoinTable: true
    #Assert: true
    breakLine:
      betweenEachAttribute: true
  php:
    version: 7.3
  signature: 'EntitiesMaker - entitiesmaker.com'
entities:
  - name: 'User'
    schema: 'Person'
    traits:
      - App\\Entity\\Traits\\UuidV6
      - App\\Entity\\Traits\\History
    #apiFilter:
    #  - type: 'OrderFilter::class'
    #    properties: null
    #apiResource:
    #      - .......
    fields:
      - name: 'username'
        dbalType: 'string'
        unique: true
        options:
          comment: 'User username'
        asserts:
          - notNull
          - Email:
            mode: 'strict'
            message: "The email '{{ value }}' is not a valid email."
          - Length:
            min: 5
            max: 255
      - name: 'firstName'
        schema: 'givenName'
        dbalType: 'string'
        options:
          comment: 'User firstname'
        asserts:
          - notBlank
          - notNull
          - Length:
            min: 2
            max: 50
      - name: 'age'
        dbalType: 'integer'
        options:
          comment: 'User age'
          default: 18
        asserts:
          - notNull
          - type: 'float'
      - name: 'price'
        dbalType: 'integer'
        options:
          comment: 'Price'
          default: 5
        asserts:
          - notBlank
          - notNull
          - type: 'float' # Possibility to override the type in the "type" assertion
      - name: 'tags'
        dbalType: 'string'
        asserts:
          - notBlank
          - Unique
      - name: 'roles'
        dbalType:
          - ManyToMany:
            target: Role
            cascade:
              persist
              delete
        options:
          comment: 'User roles'
        asserts:
          - notNull
          - Unique
        apiSubresource:
          maxDepth: 1
        #apiFilter:
        #  type: 'SearchFilter::class'
        #  properties:
        #      modelForm.authorizationNumber: 'exact'
        #      modelForm.zipCode: 'exact'
  - name: 'Role'
    traits:
      - App\\Entity\\Traits\\UuidV6
      - App\\Entity\\Traits\\History
    #apiFilter:
    #  - type: 'OrderFilter::class'
    #    properties: null
    #apiResource:
    #      - ...
    fields:
      - name: 'name'
        dbalType: 'string'
        options:
          comment: 'Role name'
          default: 'ROLE_USER'
configs:
`;