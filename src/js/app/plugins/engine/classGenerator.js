let ano = require('./annotationGenerator');
let divers = require('../utils/divers');

/**
 *
 * @param traits
 * @param tabNumber
 * @returns {{usesTrait: string, uses: string}}
 */
export let useTraits = function (traits, tabNumber = 0) {
    let usesTrait = [];
    let uses = [];

    traits.forEach(function (value, key) {
        uses.push(divers.tab(tabNumber) + 'use ' + value + 'Trait;');
        usesTrait.push(divers.tab(tabNumber) + 'use ' + value.split('\\').pop() + 'Trait;');
    });

    return {
        uses: uses.join('\n'),
        usesTrait: usesTrait.join('\n')
    };
}

/**
 *
 * @param traits
 * @param tabNumber
 * @returns {string}
 */
export let clone = function (traits, tabNumber = 0) {
    if (!Array.isArray(traits) || traits.length === 0) {
        return '';
    }

    let clones = [];
    traits.forEach(function (value, key) {
        clones.push(divers.tab(tabNumber + 1) + '$this->reset' + value.split('\\').pop() + '();');
    });

    return divers.tab(tabNumber) + '/**\n' +
        divers.tab() + ' * Reset all traits\n' +
        divers.tab() + ' */\n\tpublic function __clone()\n' +
        divers.tab(tabNumber) + '{\n' +
        clones.join('\n') + '\n' +
        divers.tab(tabNumber) + '}';
}

/**
 *
 * @param parsed
 * @param entity
 * @param traits
 * @returns {string}
 */
export let entityHeader = function (parsed, entity, traits) {
    let entityPhpCode = '<?php\n\n';

    entityPhpCode += 'declare(strict_types=1);\n\n' +
        'namespace App\\Entity;\n\n' +

        'use ApiPlatform\\Core\\Annotation\\ApiProperty;\n' +
        'use ApiPlatform\\Core\\Annotation\\ApiResource;\n' +
        'use ApiPlatform\\Core\\Annotation\\ApiSubresource;\n' +

        'use Doctrine\\Common\\Collections\\ArrayCollection;\n' +
        'use Doctrine\\Common\\Collections\\Collection;\n' +
        'use Doctrine\\ORM\\Mapping as ORM;\n' +

        'use Symfony\\Component\\Validator\\Constraints as Assert;\n' +

        'use App\\Entity\\Interfaces\\EntityHistoryInterface;\n' +
        'use App\\Entity\\Interfaces\\EntityInterface;\n' +
        'use App\\Entity\\Interfaces\\UuidInterface;\n';

    if (Array.isArray(traits) && traits.length !== 0) {
        entityPhpCode += useTraits(traits).uses + '\n';
    }

    entityPhpCode += 'use App\\Repository\\' + entity.name + 'Repository;\n' +
        '\n' +
        '/**\n'
        + ' * Class ' + entity.name + '\n'
        + ' * @package App\\Entity\n'
        + ' *\n'
        + ano.annotationEntityApiResource(entity) + '\n'
        + ' *\n'
        + ano.annotationOrmEntity(entity) + '\n'
        + ano.annotationOrmTable(entity) + '\n'
        + ano.annotationOrmHasLifecycleCallbacks(entity) + '\n'
        + ' */\n'
        + 'class ' + entity.name + ' implements EntityInterface, UuidInterface, EntityHistoryInterface\n'
        + '{\n\n'
    ;

    if (Array.isArray(traits) && traits.length !== 0) {
        entityPhpCode += useTraits(traits, 1).usesTrait + '\n\n';
    }

    return entityPhpCode;
}

/**
 *
 * @param entity
 * @param traits
 * @returns {string}
 */
export let entityFooter = function (entity, traits) {
    let entityPhpCode = '';

    if (Array.isArray(traits) && traits.length !== 0) {
        entityPhpCode += clone(traits, 1) + '\n\n';
    }

    return entityPhpCode;
}

/**
 *
 * @param parsed
 * @param entity
 * @returns {string}
 */
export let repositoryHeader = function (parsed, entity) {
    let entityPhpCode = '<?php\n\n';

    entityPhpCode += 'declare(strict_types = 1);\n' +
        '\n' +
        'namespace App\\Repository;\n' +
        '\n' +
        'use App\\Entity\\' + entity.name + ';\n' +
        'use Doctrine\\Bundle\\DoctrineBundle\\Repository\\ServiceEntityRepository;\n' +
        'use Doctrine\\Persistence\\ManagerRegistry;\n' +
        '\n' +
        '/**\n'
        + ' * Class ' + entity.name + 'Repository\n'
        + ' * @package App\\Repository\n'
        + ' *\n'
        + ' * @method ' + entity.name + '|null find(\\$id, \\$lockMode = null, \\$lockVersion = null)\n'
        + ' * @method ' + entity.name + '|null findOneBy(array \\$criteria, array \\$orderBy = null)\n'
        + ' * @method array<' + entity.name + '> findAll()\n'
        + ' * @method array<' + entity.name + '> findBy(array \\$criteria, array \\$orderBy = null, \\$limit = null, \\$offset = null)\n'
        + ' */\n' +
        'class ' + entity.name + 'Repository extends ServiceEntityRepository\n' +
        '{\n' +
        '\n' +
        '    /**\n' +
        '     * ' + entity.name + 'Repository constructor.\n' +
        '     *\n' +
        '     * @param ManagerRegistry \\$registry\n' +
        '     */\n' +
        '    public function __construct(ManagerRegistry \\$registry)\n' +
        '    {\n' +
        '        parent::__construct(\\$registry, ' + entity.name + '::class);\n' +
        '    }\n' +
        '\n';

    return entityPhpCode;
}