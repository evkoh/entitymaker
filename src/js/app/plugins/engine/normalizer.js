let divers = require('../utils/divers');

/**
 *
 * @param field
 * @returns {string}
 */
export let getRelationType = function (field) {
    return Object.keys(field.dbalType[0])[0];
}


/**
 *
 * @param field
 * @param relationName
 * @returns {boolean}
 */
export let isRelation = function (field, relationName) {
    if (!Array.isArray(field.dbalType)) {
        return false;
    }

    if (typeof relationName === 'undefined') {
        return getRelationType(field) === 'OneToOne' ||
            getRelationType(field) === 'OneToMany' ||
            getRelationType(field) === 'ManyToOne' ||
            getRelationType(field) === 'ManyToMany';
    } else {
        return getRelationType(field) === relationName;
    }
}

/**
 * Is Collection (OneToMany, ManyToMany)
 * @param field
 * @returns {boolean}
 */
export let isCollection = function isCollection(field) {
    if (!Array.isArray(field.dbalType)) {
        return false;
    }

    return getRelationType(field) === 'OneToMany' ||
        getRelationType(field) === 'ManyToMany';
}

/**
 *
 * @param parsed
 * @param entity
 * @param field
 * @returns {{currentField: {}, inversedField: null}}
 */
export let typeNormalization = function (parsed, entity, field) {
    console.log(parsed);

    let typeObject = {
        currentField: {}
    };

    typeObject.currentField.isRelation = isRelation(field);

    if (typeObject.currentField.isRelation) {
        typeObject.currentField.isCollection = isCollection(field);
        typeObject.currentField.type = getRelationType(field);
        if (typeof field.dbalType[0].cascade !== 'undefined') {
            typeObject.currentField.cascade = field.dbalType[0].cascade.split(' ');
        } else {
            typeObject.currentField.cascade = [];
        }
        typeObject.currentField.name = entity.name;
        typeObject.currentField.fqn = 'App\\Entity\\' + entity.name;
        typeObject.currentField.class = entity.name + '::class';

        typeObject.inversedField = {};
        parsed.entities.forEach(function (otherEntity) {
            if (otherEntity.name === field.dbalType[0].target) {
                typeObject.inversedField.name = otherEntity.name;
                typeObject.inversedField.fqn = 'App\\Entity\\' + otherEntity.name;
                typeObject.inversedField.class = otherEntity.name + '::class';
                typeObject.inversedField.cascade = typeObject.currentField.cascade;

                if (isRelation(field)) {
                    typeObject.inversedField.isRelation = true;
                }

                if (isRelation(field, 'ManyToMany')) {
                    typeObject.inversedField.isCollection = true;
                    typeObject.inversedField.type = 'ManyToMany';
                    typeObject.currentField.reverseProperty = divers.lowerFirstLetter(otherEntity.name + 's');
                    typeObject.inversedField.reverseProperty = divers.lowerFirstLetter(entity.name + 's');
                }

                if (isRelation(field, 'OneToOne')) {
                    typeObject.inversedField.type = 'OneToOne';
                    typeObject.currentField.reverseProperty = divers.lowerFirstLetter(otherEntity.name);
                    typeObject.inversedField.reverseProperty = divers.lowerFirstLetter(entity.name);
                }

                if (isRelation(field, 'ManyToOne')) {
                    typeObject.inversedField.type = 'OneToMany';
                    typeObject.currentField.reverseProperty = divers.lowerFirstLetter(otherEntity.name + 's');
                    typeObject.inversedField.reverseProperty = divers.lowerFirstLetter(entity.name);
                }

                if (isRelation(field, 'OneToMany')) {
                    typeObject.inversedField.type = 'ManyToOne';
                    typeObject.currentField.reverseProperty = divers.lowerFirstLetter(otherEntity.name);
                    typeObject.inversedField.reverseProperty = divers.lowerFirstLetter(entity.name + 's');
                }
            }
        });
    } else {
        typeObject.currentField.type = field.dbalType;
    }

    console.log(typeObject);

    return typeObject;
}

/**
 *
 * @param parsed
 * @param entity
 * @param field
 */
export let typeNormalizationReversed = function (parsed, entity, field) {
    return {
        currentField: field.type.inversedField,
        inversedField: field.type.currentField,
    };
}