let divers = require('../utils/divers');
let normalizer = require('./normalizer');
let s = require('../utils/shortFunctions');
let pro = require('./propertyGenerator');
let notify = require('../notify/index');
let logs = require('../notify/logs');

/**
 *
 * @param name
 * @param value
 * @returns {{name: *, type: *, value: *}}
 */
export let attribute = function (name, value) {
    let type = divers.getTypeByValue(value);

    if (type === 'json') {
        value = JSON.stringify(value);
        value = value.toString().replaceAll("'", "\\\'");
        value = value.toString().replaceAll('"', "'");
    } else if (type === 'array') {
        let json = '{';

        value.forEach(function (val, key) {
            json += divers.displayValueForType(val) + ',';
        });

        // Remove last comma
        json = json.replace(/,\s*$/, "");

        json += '}';

        value = json;
        type = 'json';
    }

    return {
        name: name,
        type: type,
        value: value,
    };
}

export let annotationGenerator = function (name, attributes) {
    return {
        name: name,
        attributes: attributes,
    };
}

/**
 *
 * @param annotationObject
 * @param tabNumber
 * @param tabNumberAfter
 * @returns {string}
 */
export let toString = function (annotationObject, tabNumber = 0, tabNumberAfter = 0) {
    let withBreakLine = parsed.settings.annotations.breakLine.betweenEachAttribute;
    let annotationString = divers.tab(tabNumber) + ' * ' + divers.tab(tabNumberAfter) + '@' + annotationObject.name;

    // Attributes
    if (annotationObject.attributes.length > 0) {
        // Start
        annotationString += '(';

        // Each attributes
        let attributesString = [];
        annotationObject.attributes.forEach(function (attribute) {
            attributesString.push(attribute.name + '=' + divers.displayValueForType(attribute.value, false, attribute.type));
        });
        if (withBreakLine) {
            let beginAttributeLine = '\n' + divers.tab(tabNumber) + ' * ' + divers.tab(tabNumberAfter) + divers.tab(1);
            annotationString += beginAttributeLine + attributesString.join(',' + beginAttributeLine);
        } else {
            annotationString += attributesString.join(', ');
        }

        // End
        if (withBreakLine) {
            annotationString += '\n' + divers.tab(tabNumber) + ' * ' + divers.tab(tabNumberAfter) + ')';
        } else {
            annotationString += ')';
        }
    }

    return annotationString;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let annotationOrmColumn = function (field, tabNumber = 0) {
    let attributes = [];

    attributes.push(attribute('name', field.name));
    attributes.push(attribute('type', field.type.currentField.type));
    attributes.push(attribute('nullable', !s.hasAssert(field.asserts, 'notNull', true)));

    // Options
    if (typeof field.options !== 'undefined') {
        attributes.push(attribute('options', field.options));
    }

    // Unique
    if (typeof field.unique !== 'undefined' && s.inObjectWithValue(field, 'unique', true)) {
        attributes.push(attribute('unique', true));
    } else {
        attributes.push(attribute('unique', false));
    }

    // Length
    s.getAssert(field.asserts, 'Length', function (assertLength) {
        attributes.push(attribute('length', assertLength.max));
    });

    return toString(annotationGenerator('ORM\\Column', attributes), tabNumber) + "\n";
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let annotationApiSubresource = function (field, tabNumber = 0) {
    let attributes = [];

    if (typeof field.apiSubresource !== 'undefined' && typeof field.apiSubresource.maxDepth !== 'undefined') {
        attributes.push(attribute('maxDepth', field.apiSubresource.maxDepth));
    }

    return toString(annotationGenerator('ApiSubresource', attributes), tabNumber) + "\n";
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {*}
 */
export let annotationAssertNotBlank = function (field, tabNumber = 0) {
    let entityPhpCode = '';
    let attributes = [];

    // https://symfony.com/doc/current/reference/constraints/NotBlank.html#allownull

    if (s.hasAssert(field.asserts, 'notBlank')) {
        if (s.hasAssert(field.asserts, 'notNull')) {
            attributes.push(attribute('allowNull', false));
        } else {
            attributes.push(attribute('allowNull', true));
        }
        attributes.push(attribute('message', field.name + ' should not be blank.'));
        // groups
        // normalizer
        // payload

        entityPhpCode += toString(annotationGenerator('Assert\\NotBlank', attributes), tabNumber) + "\n";
    }

    return entityPhpCode;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {*}
 */
export let annotationAssertType = function (field, tabNumber = 0) {
    let entityPhpCode = '';
    let attributes = [];

    // https://symfony.com/doc/current/reference/constraints/Type.html

    let type;
    if (s.hasAssert(field.asserts, 'type')) {
        s.getAssert(field.asserts, 'type', function (assertType) {
            type = assertType.type;
        });
    } else {
        if (field.type.currentField.isRelation) {
            type = field.type.inversedField.fqn;
        } else {
            type = field.type.currentField.type;
        }
    }

    attributes.push(attribute('type', type));
    attributes.push(attribute('message', 'The value of ' + field.name + ' should be of ' + type + '.'));
    // groups
    // payload

    if (field.type.currentField.isCollection) {
        entityPhpCode += divers.tab(tabNumber) + ' * @Assert\\All({\n';
        entityPhpCode += toString(annotationGenerator('Assert\\Type', attributes), tabNumber, 1) + "\n";
        entityPhpCode += divers.tab(tabNumber) + ' * })\n';
    } else {
        entityPhpCode += toString(annotationGenerator('Assert\\Type', attributes), tabNumber) + "\n";
    }


    return entityPhpCode;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {*}
 */
export let annotationAssertValid = function (field, tabNumber = 0) {
    let entityPhpCode = '';

    // https://symfony.com/doc/current/reference/constraints/Valid.html

    // traverse
    // groups
    // payload

    entityPhpCode += toString(annotationGenerator('Assert\\Valid', []), tabNumber) + "\n";

    return entityPhpCode;
}

/**
 *
 * @param field
 * @param assert
 * @param tabNumber
 * @returns {*}
 */
export let annotationAssertOther = function (field, assert, tabNumber = 0) {
    let entityPhpCode = '';
    let attributes = [];

    for (const [key, value] of Object.entries(assert)) {
        if (key !== assert.metadata.name && key !== 'metadata') {
            attributes.push(attribute(key, value));
        }
    }

    entityPhpCode += toString(annotationGenerator('Assert\\' + assert.metadata.name, attributes), tabNumber) + "\n";

    return entityPhpCode;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {*}
 */
export let annotationAssertNotNull = function (field, tabNumber = 0) {
    let entityPhpCode = '';
    let attributes = [];

    // https://symfony.com/doc/current/reference/constraints/NotNull.html

    if (s.hasAssert(field.asserts, 'notNull')) {
        attributes.push(attribute('message', field.name + ' should not be null.'));
        // groups
        // payload

        entityPhpCode += toString(annotationGenerator('Assert\\NotNull', attributes), tabNumber) + "\n";
    }

    return entityPhpCode;
}

/**
 *
 * @param entity
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let annotationVar = function (entity, field, tabNumber = 0) {
    let entityPhpCode = '@var ';

    let allowNull = !s.hasAssert(field.asserts, 'notNull');

    if (field.type.currentField.isRelation) {
        entityPhpCode += 'ArrayCollection|array<' + field.type.inversedField.name + '>';

        if (allowNull) {
            logs.error('AV.001',
                'The field : <b>' + field.name + '</b> of entity <b>' + entity.name +
                '</b> cannot be a relation and null at the same time, we must add the <bg>notNull</bg> assert !',
                'top right'
            );
        }
    } else {
        entityPhpCode += field.type.currentField.type;
    }

    // Allow Null
    if (allowNull) {
        entityPhpCode += '|null';
    }

    return divers.tab(tabNumber) + ' * ' + entityPhpCode + '\n';
}

/**
 *
 * @param tabNumber
 * @returns {string}
 */
export let annotationsStart = function (tabNumber = 0) {
    return divers.tab(tabNumber) + '/**\n';
}

/**
 *
 * @param tabNumber
 * @returns {string}
 */
export let annotationsEnd = function (tabNumber = 0) {
    return divers.tab(tabNumber) + ' */\n';
}

/**
 *
 * @param entity
 * @param tabNumber
 * @returns {string}
 */
export let annotationEntityApiResource = function (entity, tabNumber = 0) {
    let entityPhpCode = '';
    let apiResourceAttributes = [];

    apiResourceAttributes.push(attribute('itemOperations', ['get', 'put', 'patch', 'delete']));
    apiResourceAttributes.push(attribute('collectionOperations', ['get', 'post']));
    apiResourceAttributes.push(attribute('graphql', {
        "create": {
            "mutation": entity.name + 'MutationResolver::class'
        },
        "update": {
            "mutation": entity.name + 'MutationResolver::class'
        }
    }));

    if (typeof entity.iri !== 'undefined') {
        apiResourceAttributes.push(attribute('iri', entity.iri));
    }

    entityPhpCode += toString(annotationGenerator(
        'ApiResource',
        apiResourceAttributes
    ))

    return divers.tab(tabNumber) + entityPhpCode;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let fieldApiProperty = function (field, tabNumber = 0) {
    let entityPhpCode = '';
    let apiPropertyAttributes = [];

    if (typeof field.iri !== 'undefined') {
        apiPropertyAttributes.push(attribute('iri', field.iri));
    }

    if (typeof field.schema !== 'undefined') {
        apiPropertyAttributes.push(attribute('iri', 'https://schema.org/' + field.schema));
    }

    if (typeof field.options !== 'undefined' && typeof field.options.comment !== 'undefined') {
        apiPropertyAttributes.push(attribute('description', field.options.comment));
    }

    if (s.hasAssert(field.asserts, 'notNull')) {
        apiPropertyAttributes.push(attribute('required', true));
    } else {
        apiPropertyAttributes.push(attribute('required', false));
    }

    entityPhpCode += toString(annotationGenerator(
        'ApiProperty',
        apiPropertyAttributes
    ), tabNumber);

    return entityPhpCode + '\n';
}

/**
 *
 * @param entity
 * @param tabNumber
 * @returns {string}
 */
export let annotationOrmEntity = function (entity, tabNumber = 0) {
    let entityPhpCode = '';

    entityPhpCode += toString(annotationGenerator(
        'ORM\\Entity',
        [
            attribute('repositoryClass', entity.name + 'Repository::class')
        ]
    ))

    return divers.tab(tabNumber) + entityPhpCode;
}

/**
 *
 * @param entity
 * @param tabNumber
 * @returns {string}
 */
export let annotationOrmTable = function (entity, tabNumber = 0) {
    let entityPhpCode = '';

    entityPhpCode += toString(annotationGenerator(
        'ORM\\Table',
        [
            attribute('name', entity.name)
        ]
    ))

    return divers.tab(tabNumber) + entityPhpCode;
}

/**
 *
 * @param entity
 * @param tabNumber
 * @returns {string}
 */
export let annotationOrmHasLifecycleCallbacks = function (entity, tabNumber = 0) {
    let entityPhpCode = '';

    entityPhpCode += toString(annotationGenerator(
        'ORM\\HasLifecycleCallbacks',
        []
    )) + '()'

    return divers.tab(tabNumber) + entityPhpCode;
}

/**
 * Bidirectional, Unidirectional, Self-referencing
 * @param entity
 * @param field
 * @param tabNumber
 * @param isReverseRelationPart
 */
export let annotationOrmOneToOne = function (entity, field, tabNumber = 0, isReverseRelationPart = false) {
    // todo coder ça
}

/**
 * Unidirectional
 * @param entity
 * @param field
 * @param tabNumber
 * @param isReverseRelationPart
 */
export let annotationOrmManyToOne = function (entity, field, tabNumber = 0, isReverseRelationPart = false) {
    // todo coder ça
}

/**
 * Bidirectional, Unidirectional with Join Table, Self-referencing
 * @param entity
 * @param field
 * @param tabNumber
 * @param isReverseRelationPart
 */
export let annotationOrmOneToMany = function (entity, field, tabNumber = 0, isReverseRelationPart = false) {
    // todo coder ça
}

/**
 * Bidirectional, Unidirectional, Self-referencing
 * @param entity
 * @param field
 * @param tabNumber
 * @param isReverseRelationPart
 * @returns {string}
 */
export let annotationOrmManyToMany = function (entity, field, tabNumber = 0, isReverseRelationPart = false) {
    let entityPhpCode = '';

    // @ManyToMany
    let manyToManyAttributes = [];
    manyToManyAttributes.push(attribute('targetEntity', field.type.inversedField.class));
    if (typeof field.type.currentField.cascade !== 'undefined') {
        manyToManyAttributes.push(attribute('cascade', field.type.currentField.cascade));
    }
    if (false === isReverseRelationPart) {
        manyToManyAttributes.push(attribute('inversedBy', divers.lowerFirstLetter(entity.name) + 's'));
    } else {
        manyToManyAttributes.push(attribute('mappedBy', divers.lowerFirstLetter(entity.name) + 's'));
    }
    entityPhpCode += toString(annotationGenerator('ORM\\ManyToMany', manyToManyAttributes), tabNumber) + '\n'

    // @JoinTable
    if (false === isReverseRelationPart) {
        let joinTableAttributes = [];
        joinTableAttributes.push(attribute('name', divers.lowerFirstLetter(entity.name) + '_' + divers.lowerFirstLetter(field.type.inversedField.name)));
        joinTableAttributes.push(attribute('joinColumns', [
            '@ORM\\JoinColumn(name="' + divers.lowerFirstLetter(entity.name) + '_id", referencedColumnName="id")'
        ]));
        joinTableAttributes.push(attribute('inverseJoinColumns', [
            '@ORM\\JoinColumn(name="' + divers.lowerFirstLetter(field.type.inversedField.name) + '_id", referencedColumnName="id")'
        ]));
        entityPhpCode += toString(annotationGenerator('ORM\\JoinTable', joinTableAttributes), tabNumber) + '\n'
    }

    return entityPhpCode;
}