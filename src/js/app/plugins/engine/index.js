import {initStoreCommonEntity} from "./functionGenerator";

const yaml = require('js-yaml');
let divers = require('../utils/divers');
let normalizer = require('./normalizer');
let ano = require('./annotationGenerator');
let cla = require('./classGenerator');
let fn = require('./functionGenerator');
let pro = require('./propertyGenerator');
let fileManager = require('../utils/fileManager');
let localStorageManager = require('../utils/localstorage');
let s = require('../utils/shortFunctions');
let logs = require('../notify/logs');

/**
 *
 * @param yamlString
 */
export let parseYaml = function (yamlString) {
    yamlString = yamlString.replace("\t", '  ');
    logs.clearLogs();

    let entityPhpCode;
    let parsed = yaml.load(yamlString);
    window.parsed = parsed;
    window.phpFiles = {};
    window.storeCommonEntityContent = {};

    fileManager.remove('/workspace/src');
    fileManager.addDirRecursive('/workspace/src/Entity/Traits');
    fileManager.addDirRecursive('/workspace/src/Entity/Interfaces');
    fileManager.addDirRecursive('/workspace/src/Repository');
    fileManager.addDirRecursive('/workspace/src/Doctrine');
    fileManager.addDirRecursive('/workspace/src/Doctrine/IdGenerator');
    fileManager.addDirRecursive('/workspace/src/ApiPlatform/Filter/Doctrine/Orm');
    fileManager.addDirRecursive('/workspace/src/Exception/Api');

    let prepareFiles = function (content) {
        return fileManager.preparePhpFile(content);
    };

    fileManager.addFileFromFileGetContent('/Entity/Traits/UuidV4Trait.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Entity/Traits/UuidV6Trait.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Doctrine/IdGenerator/UuidV4Generator.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Doctrine/IdGenerator/UuidV6Generator.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Entity/Interfaces/EntityInterface.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Entity/Interfaces/UuidInterface.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Entity/Interfaces/EntityHistoryInterface.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/ApiPlatform/Filter/Doctrine/Orm/ExtensibleFilter.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/ApiPlatform/Filter/Doctrine/Orm/UlidBinaryFilter.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/ApiPlatform/Filter/Doctrine/Orm/UlidFilter.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/ApiPlatform/Filter/Doctrine/Orm/UuidBinaryFilter.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/ApiPlatform/Filter/Doctrine/Orm/UuidFilter.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Exception/Api/AbstractApiException.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Exception/Api/ApiExceptionInterface.php', prepareFiles);
    fileManager.addFileFromFileGetContent('/Exception/Api/InvalidObjectException.php', prepareFiles);

    // 1ère passe, on store le contenu des différentes parties en mémoire
    parsed.entities.forEach(function (entity, entityKey) {

        let traits = [];
        if (typeof entity.traits !== 'undefined') {
            entity.traits.forEach(function (traitName) {
                traits.push(traitName);
            });
        }
        fn.storeCommonEntity(entity.name, entity.name, 'headers', cla.entityHeader(parsed, entity, traits));

        // Properties
        window.arrayCollections = [];
        entity.fields.forEach(function (field, fieldKey) {
            parsed.entities[entityKey].fields[fieldKey].type = normalizer.typeNormalization(parsed, entity, field);
            delete parsed.entities[entityKey].fields[fieldKey].dbalType;

            // Current fiels property
            let propertiesPhpCode = pro.propertyMaker(entity, field, false);

            pro.storeReverseRelationPartProperties(parsed, entity, field); // Revers Relation Part of property
            fn.storeCommonEntity(entity.name, entity.name, 'properties', propertiesPhpCode);  // Current property
        });

        // Constructor
        let constructorPhpCode = fn.createConstructor(entity.name, window[entity.name].arrayCollections, 1);
        fn.storeCommonEntity(entity.name, entity.name, 'contentConstructor', constructorPhpCode); // Current property functions

        // Getter / Setter / Adder / Remover
        entity.fields.forEach(function (field) {
            let functionsPhpCode = '';
            if (field.type.currentField.isRelation && field.type.currentField.isCollection) {
                // Current field functions
                functionsPhpCode += fn.getter(field, 1);
                functionsPhpCode += fn.setter(entity, field, 1);
                functionsPhpCode += fn.adder(entity, field, 1);
                functionsPhpCode += fn.remover(entity, field, 1);
                fn.storeTargetFunctions(parsed, entity, field); // Target field functions

                if (typeof window[field.type.inversedField.name] === 'undefined') {
                    window[field.type.inversedField.name] = {
                        arrayCollections: []
                    };
                }

                //window[field.type.inversedField.name].arrayCollections.push(field.type.currentField.reverseProperty);
            } else {
                functionsPhpCode += fn.getter(field, 1);
                functionsPhpCode += fn.setter(entity, field, 1);
            }
            fn.storeCommonEntity(entity.name, entity.name, 'functions', functionsPhpCode); // Current property functions
        });

        fn.storeCommonEntity(entity.name, entity.name, 'footers', cla.entityFooter(entity, traits));
    });

    console.log(parsed.entities);

    // 2ème passe sur les entités afin de gérer les ajouts provenant d'autres entités
    parsed.entities.forEach(function (entity) {
        // Header
        entityPhpCode = fn.fromStoreCommonEntity(entity.name, 'headers');

        // Morceau 'properties' de code provenant d'une autre classe (autre partie des relations)
        entityPhpCode += fn.fromStoreCommonEntity(entity.name, 'properties');

        // Morceau 'contentConstructor' de code provenant d'une autre classe (autre partie des relations)
        entityPhpCode += fn.fromStoreCommonEntity(entity.name, 'contentConstructor');

        // Morceau 'functions' de code provenant d'une autre classe (autre partie des relations)
        entityPhpCode += fn.fromStoreCommonEntity(entity.name, 'functions');

        // Footer
        entityPhpCode += fn.fromStoreCommonEntity(entity.name, 'footers');

        window.phpFiles['/workspace/src/Repository/' + entity.name + 'Repository.php'] = cla.repositoryHeader(parsed, entity);
        window.phpFiles['/workspace/src/Entity/' + entity.name + '.php'] = entityPhpCode;
    });

    // 3ème passe sur les entités afin d'écrire dans les fichiers à présent
    for (let [filePath, content] of Object.entries(window.phpFiles)) {
        content += '}';

        fileManager.addFile(filePath, fileManager.preparePhpFile(content));
    }
}