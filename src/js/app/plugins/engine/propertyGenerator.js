import {attribute} from "./annotationGenerator";
import {storeCommonEntity} from "./functionGenerator";

let divers = require('../utils/divers')
let s = require('../utils/shortFunctions');
let ano = require('./annotationGenerator');
let normalizer = require('./normalizer');

/**
 *
 * @param field
 * @param defaultValue
 * @param tabNumber
 * @returns {string}
 */
export let toString = function (field, defaultValue, tabNumber) {
    let varName = divers.lowerFirstLetter(field.name);
    let visibility = 'private';

    // Si la valeur par défaut est null ou si le champs est nullable
    if (defaultValue === null || s.hasAssert(field.asserts, 'notNull')) {
        return divers.tab(tabNumber) + visibility + ' $' + varName + ';';
    } else {
        return divers.tab(tabNumber) + visibility + ' $' + varName + ' = ' + divers.displayValueForType(defaultValue, true) + ';';
    }
}

/**
 *
 * @param entity
 * @param field
 * @param isReverseRelationPart
 * @returns {string}
 */
export let propertyMaker = function (entity, field, isReverseRelationPart = false) {
    let entityPhpCode = '';

    entityPhpCode += ano.annotationsStart(1);

    if (typeof field.asserts !== 'undefined') {
        field.asserts = s.uniformizeArray(field.asserts);
    }

    // Annotation @var
    entityPhpCode += ano.annotationVar(entity, field, 1);

    // Annotation ApiProperty
    entityPhpCode += ano.fieldApiProperty(field, 1);

    // Assert : Not Blank
    entityPhpCode += ano.annotationAssertNotBlank(field, 1);

    // Assert : Not Null
    entityPhpCode += ano.annotationAssertNotNull(field, 1);

    // Assert : Type
    entityPhpCode += ano.annotationAssertType(field, 1);

    // Assert : Valid
    entityPhpCode += ano.annotationAssertValid(field, 1);

    // Others asserts : https://symfony.com/doc/current/validation.html#supported-constraints
    if (typeof field.asserts !== 'undefined') {
        field.asserts.forEach(function (assert) {
            if (!s.inArray(['notBlank', 'notNull', 'type'], assert.metadata.name)) {
                entityPhpCode += ano.annotationAssertOther(field, assert, 1);
            }
        });
    }

    if (field.type.currentField.isRelation) {
        if (field.type.currentField.type === 'OneToOne') {
            // Annotation OneToOne
            entityPhpCode += ano.annotationOrmOneToOne(entity, field, 1, isReverseRelationPart);
        } else if (field.type.currentField.type === 'ManyToOne') {
            // Annotation ManyToOne
            entityPhpCode += ano.annotationOrmManyToOne(entity, field, 1, isReverseRelationPart);
        }

        // Annotation Subresource
        entityPhpCode += ano.annotationApiSubresource(field, 1);

        if (field.type.currentField.isCollection) {
            if (field.type.currentField.type === 'OneToMany') {
                // Annotation OneToMany
                entityPhpCode += ano.annotationOrmOneToMany(entity, field, 1, isReverseRelationPart);
            } else if (field.type.currentField.type === 'ManyToMany') {
                // Annotation ManyToMany
                entityPhpCode += ano.annotationOrmManyToMany(entity, field, 1, isReverseRelationPart);
            }

            if (typeof window[entity.name] === 'undefined') {
                window[entity.name] = {
                    arrayCollections: []
                };
            }

            window[entity.name].arrayCollections.push(field.name);
        }
    } else {
        // Annotation ORM\Column
        entityPhpCode += ano.annotationOrmColumn(field, 1);
    }

    entityPhpCode += ano.annotationsEnd(1);

    // Property
    entityPhpCode += toString(field, s.inObjectDefaultValue(field.options, 'default'), 1) + "\n\n";

    return entityPhpCode;
}

/**
 *
 * @param parsed
 * @param entity
 * @param field
 */
export let storeReverseRelationPartProperties = function (parsed, entity, field) {
    if (field.type.currentField.isRelation) {
        let entityPhpCode = '';

        let asserts = [];

        // Si la propriété de l'autre coté de la relation est une collection alors elle ne peut etre null
        if (field.type.currentField.isCollection) {
            asserts.push('notNull');
        }

        let targetField = {
            name: field.type.inversedField.reverseProperty,
            type: normalizer.typeNormalizationReversed(parsed, entity, field),
            options: '',
            asserts: asserts,
        };

        // ApiSubresource
        if (typeof field.apiSubresource !== 'undefined') {
            targetField.apiSubresource = field.apiSubresource;
        }

        entityPhpCode += propertyMaker(field.type.inversedField, targetField, true);
        storeCommonEntity(field.type.inversedField.name, entity.name, 'properties', entityPhpCode);
    }
}