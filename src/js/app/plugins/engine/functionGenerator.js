let divers = require('../utils/divers');
let s = require('../utils/shortFunctions');
let pr = require('./propertyGenerator');
let normalizer = require('./normalizer');

/**
 *
 * @param className
 * @param arrayCollections
 * @param tabNumber
 * @returns {string}
 */
export let createConstructor = function (className, arrayCollections = [], tabNumber = 0) {
    let phpCode = divers.tab(tabNumber) + '/**\n' +
        divers.tab(tabNumber) + ' * ' + className + ' constructor.\n' +
        divers.tab(tabNumber) + ' */\n' +
        divers.tab(tabNumber) + 'public function __construct()\n' +
        divers.tab(tabNumber) + '{\n';

    arrayCollections.forEach(function (propertyName) {
        phpCode += divers.tab(tabNumber + 1) + '$this->' + propertyName + ' = new ArrayCollection();\n';
    });

    phpCode += divers.tab(tabNumber) + '}\n\n';

    return phpCode;
}

/**
 *
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let getter = function (field, tabNumber = 0) {
    let propertyName = field.name;
    let ucFirstPropertyName = divers.capitalizeFirstLetter(propertyName);
    let nullableAnnotation = '';
    let nullableFunction = '';
    let returnType;
    let returnTypeAnnotation;

    if (field.type.currentField.isRelation) {
        returnType = 'Collection';
        returnTypeAnnotation = 'array<' + field.type.inversedField.name + '>|Collection';
    } else {
        returnType = field.type.currentField.type;
        returnTypeAnnotation = field.type.currentField.type;
    }

    if (s.hasAssert(field.asserts, 'notNull')) {
        nullableAnnotation = '|null';
        nullableFunction = '?';
    }

    return divers.tab(tabNumber) + '/**\n' +
        divers.tab(tabNumber) + ' * @return ' + returnTypeAnnotation + nullableAnnotation + '\n' +
        divers.tab(tabNumber) + ' */\n' +
        divers.tab(tabNumber) + 'public function get' + ucFirstPropertyName + '(): ' + nullableFunction + returnType + '\n' +
        divers.tab(tabNumber) + '{\n' +
        divers.tab(tabNumber + 1) + 'return $this->' + propertyName + ';\n' +
        divers.tab(tabNumber) + '}\n\n';
}

/**
 *
 * @param entity
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let setter = function (entity, field, tabNumber = 0) {
    let propertyName = field.name;
    let ucFirstPropertyName = divers.capitalizeFirstLetter(propertyName);
    let nullableAnnotation = '';
    let nullableFunction = '';
    let defaultValue = '';
    let typeParam;
    let typeAnnotation;

    if (field.type.currentField.isRelation) {
        typeParam = 'ArrayCollection';
        typeAnnotation = 'array<' + field.type.inversedField.name + '>|ArrayCollection';
    } else {
        typeParam = field.type.currentField.type;
        typeAnnotation = field.type.currentField.type;
    }

    if (s.hasAssert(field.asserts, 'notNull')) {
        nullableAnnotation = '|null';
        nullableFunction = '?';
    }

    if (typeof field.options !== 'undefined' && typeof field.options.default !== 'undefined') {
        defaultValue = ' = ' + divers.displayValueForType(field.options.default);
    }

    return divers.tab(tabNumber) + '/**\n' +
        divers.tab(tabNumber) + ' * @param ' + typeAnnotation + nullableAnnotation + ' $' + propertyName + '\n' +
        divers.tab(tabNumber) + ' *\n' +
        divers.tab(tabNumber) + ' * @return ' + entity.name + '\n' +
        divers.tab(tabNumber) + ' */\n' +
        divers.tab(tabNumber) + 'public function set' + ucFirstPropertyName + '(' + nullableFunction + typeParam + ' $' + propertyName + defaultValue + '): self\n' +
        divers.tab(tabNumber) + '{\n' +
        divers.tab(tabNumber + 1) + '$this->' + propertyName + ' = $' + propertyName + ';\n\n' +
        divers.tab(tabNumber + 1) + 'return $this;\n' +
        divers.tab(tabNumber) + '}\n\n';
}

/**
 *
 * @param entity
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let adder = function (entity, field, tabNumber = 0) {
    let varNameCollection = divers.lowerFirstLetter(field.name);
    let varNameSingle = divers.lowerFirstLetter(field.type.inversedField.name);

    return divers.tab(tabNumber) + '/**\n' +
        divers.tab(tabNumber) + ' * @param ' + field.type.inversedField.name + ' $' + varNameSingle + '\n' +
        divers.tab(tabNumber) + ' *\n' +
        divers.tab(tabNumber) + ' * @return ' + entity.name + '\n' +
        divers.tab(tabNumber) + ' */\n' +
        divers.tab(tabNumber) + 'public function add' + divers.capitalizeFirstLetter(varNameSingle) + '(' + field.type.inversedField.name + ' $' + varNameSingle + '): self\n' +
        divers.tab(tabNumber) + '{\n' +
        divers.tab(tabNumber + 1) + 'if (!$this->' + varNameCollection + '->contains($' + varNameSingle + ')) {\n' +
        divers.tab(tabNumber + 2) + '$this->' + varNameCollection + '[] = $' + varNameSingle + ';\n' +
        divers.tab(tabNumber + 2) + '$' + varNameSingle + '->add' + entity.name + '($this);\n' +
        divers.tab(tabNumber + 1) + '}\n' +
        divers.tab(tabNumber + 1) + '\n' +
        divers.tab(tabNumber + 1) + 'return $this;\n' +
        divers.tab(tabNumber) + '}\n\n';
}

/**
 *
 * @param entity
 * @param field
 * @param tabNumber
 * @returns {string}
 */
export let remover = function (entity, field, tabNumber = 0) {
    let varNameCollection = divers.lowerFirstLetter(field.name);
    let varNameSingle = divers.lowerFirstLetter(field.type.inversedField.name);

    let phpCode = divers.tab(tabNumber) + '/**\n' +
        divers.tab(tabNumber) + ' * @param ' + field.type.inversedField.name + ' $' + varNameSingle + '\n' +
        divers.tab(tabNumber) + ' *\n' +
        divers.tab(tabNumber) + ' * @return ' + entity.name + '\n' +
        divers.tab(tabNumber) + ' */\n' +
        divers.tab(tabNumber) + 'public function remove' + field.type.inversedField.name + '(' + field.type.inversedField.name + ' $' + varNameSingle + '): self\n' +
        divers.tab(tabNumber) + '{\n' +
        divers.tab(tabNumber + 1) + 'if ($this->' + varNameCollection + '->contains($' + varNameSingle + ')) {\n' +
        divers.tab(tabNumber + 2) + '$this->' + varNameCollection + '->removeElement($' + varNameSingle + ');\n\n' +
        divers.tab(tabNumber + 2) + '// set the owning side to null (unless already changed)\n';

    if (field.type.inversedField.isCollection === true) {
        phpCode += divers.tab(tabNumber + 2) + 'if ($' + varNameSingle + '->get' + entity.name + 's()->contains($this)) {\n' +
            divers.tab(tabNumber + 3) + '$' + varNameSingle + '->remove' + entity.name + '($this);\n';
    } else {
        phpCode += divers.tab(tabNumber + 2) + 'if ($' + varNameSingle + '->get' + entity.name + '() === $this) {\n' +
            divers.tab(tabNumber + 3) + '$' + varNameSingle + '->set' + entity.name + '(null);\n';
    }

    phpCode += divers.tab(tabNumber + 2) + '}\n' +
        divers.tab(tabNumber + 1) + '}\n' +
        divers.tab(tabNumber + 1) + '\n' +
        divers.tab(tabNumber + 1) + 'return $this;\n' +
        divers.tab(tabNumber) + '}\n\n';

    return phpCode;
}

/**
 *
 * @param entityName
 * @param fromEntityName
 */
export let initStoreCommonEntity = function (entityName, fromEntityName) {
    if (typeof window.storeCommonEntityContent === 'undefined') {
        window.storeCommonEntityContent = {};
    }

    if (typeof window.storeCommonEntityContent[entityName] === 'undefined') {
        window.storeCommonEntityContent[entityName] = {};
    }

    if (typeof fromEntityName !== 'undefined' && typeof window.storeCommonEntityContent[entityName][fromEntityName] === 'undefined') {
        window.storeCommonEntityContent[entityName][fromEntityName] = {};
    }
}

/**
 *
 * @param entityName
 * @param fromEntityName
 * @param key = properties || contentConstructor || functions
 * @param content
 */
export let storeCommonEntity = function (entityName, fromEntityName, key, content) {
    initStoreCommonEntity(entityName, fromEntityName);

    if (typeof entityName === 'undefined') {
        debugger;
    }

    if (typeof fromEntityName === 'undefined') {
        debugger;
    }

    if (typeof window.storeCommonEntityContent[entityName][fromEntityName][key] === 'undefined') {
        window.storeCommonEntityContent[entityName][fromEntityName][key] = [];
    }

    window.storeCommonEntityContent[entityName][fromEntityName][key].push(content);
}

/**
 *
 * @param entityName
 * @param key = properties || contentConstructor || functions
 * @returns {*}
 */
export let fromStoreCommonEntity = function (entityName, key) {
    initStoreCommonEntity(entityName);
    let content = '';

    // D'abord, on affiche les property, functions ce cette même entité
    for (const [keyFromEntityName, fromEntityName] of Object.entries(window.storeCommonEntityContent[entityName])) {
        if (entityName === keyFromEntityName) {
            if (typeof fromEntityName[key] !== 'undefined') {
                fromEntityName[key].forEach(function (val, key) {
                    content += val;
                });
            }
        }
    }

    // En second temps, on affiche les property, functions provenant d'autres entités (autre partie des relations)
    for (const [keyFromEntityName, fromEntityName] of Object.entries(window.storeCommonEntityContent[entityName])) {
        if (entityName !== keyFromEntityName) {
            if (typeof fromEntityName[key] !== 'undefined') {
                fromEntityName[key].forEach(function (val, key) {
                    content += val;
                });
            }
        }
    }

    return content;
}

/**
 *
 * @param parsed
 * @param entity
 * @param field
 */
export let storeTargetFunctions = function (parsed, entity, field) {
    if (field.type.currentField.isRelation) {
        let entityPhpCode = '';

        let targetField = {
            name: field.type.inversedField.reverseProperty,
            type: normalizer.typeNormalizationReversed(parsed, entity, field),
            options: '',
            asserts: [],
        };

        entityPhpCode += getter(targetField, 1);
        entityPhpCode += setter(field.type.inversedField, targetField, 1);
        entityPhpCode += adder(field.type.inversedField, targetField, 1);
        entityPhpCode += remover(field.type.inversedField, targetField, 1);

        storeCommonEntity(field.type.inversedField.name, entity.name, 'functions', entityPhpCode);
    }
}