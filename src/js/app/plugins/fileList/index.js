let fileManager = require('../utils/fileManager');
let notify = require('../notify/index');

// get all folders in our .directory-list
export let initFileList = function () {
    let allFolders = $(".directory-list li > ul");

    allFolders.each(function () {
        // add the folder class to the parent <li>
        let folderAndName = $(this).parent();
        //folderAndName.addClass("folder");

        // backup this inner <ul>
        let backupOfThisFolder = $(this);

        // then delete it
        $(this).remove();

        // add an <a> tag to whats left ie. the folder name
        folderAndName.wrapInner("<a href='#' />");

        // then put the inner <ul> back
        folderAndName.append(backupOfThisFolder);

        // $('li.type-file > a').each(function () {
        //     let element = $(this);
        //     element.addClass('fileLink');
        // });

        // now add a slideToggle to the <a> we just added
        folderAndName.find("a").click(function (event) {
            let element = $(this);
            let parent = $(element).parent();

            // If is dir :
            if (parent.hasClass('type-dir')) {
                $(this).siblings("ul").slideToggle('fast', 'linear');
            }

            // If is file :
            if (parent.hasClass('type-file')) {
                fileManager.openFileByPath(parent.attr('path'));
            }

            event.preventDefault();
        });
    });
}