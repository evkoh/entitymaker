<?php

declare(strict_types = 1);

namespace App\Doctrine\IdGenerator;

use App\Entity\Interfaces\UuidInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Symfony\Component\Uid\UuidV4;

/**
 * Class UuidV4Generator
 * @package App\Doctrine\IdGenerator
 */
final class UuidV4Generator extends AbstractIdGenerator
{
    /**
     * @param EntityManager      $em
     * @param UuidInterface|null $entity
     *
     * @return UuidV4
     */
    public function generate(EntityManager $em, $entity): UuidV4
    {
        if (null !== $entity->getId()) {
            /** @var UuidV4 $uuid */
            $uuid = $entity->getId();

            return new UuidV4($uuid->toRfc4122());
        }

        return new UuidV4();
    }
}
