<?php

declare(strict_types = 1);

namespace App\Doctrine\IdGenerator;

use App\Entity\Interfaces\UlidInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Symfony\Component\Uid\Ulid;

/**
 * Class UlidGenerator
 * @package App\Doctrine\IdGenerator
 */
final class UlidGenerator extends AbstractIdGenerator
{
    /**
     * @param EntityManager      $em
     * @param UlidInterface|null $entity
     *
     * @return Ulid
     */
    public function generate(EntityManager $em, $entity): Ulid
    {
        if (null !== $entity->getId()) {
            /** @var Ulid $ulid */
            $ulid = $entity->getId();

            return new Ulid($ulid->toRfc4122());
        }

        return new Ulid();
    }
}
