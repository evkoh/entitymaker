<?php

declare(strict_types = 1);

namespace App\Doctrine\IdGenerator;

use App\Entity\Interfaces\UuidInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Symfony\Component\Uid\UuidV6;

/**
 * Class UuidV6Generator
 * @package App\Doctrine\IdGenerator
 */
final class UuidV6Generator extends AbstractIdGenerator
{
    /**
     * @param EntityManager      $em
     * @param UuidInterface|null $entity
     *
     * @return UuidV6
     */
    public function generate(EntityManager $em, $entity): UuidV6
    {
        if (null !== $entity->getId()) {
            /** @var UuidV6 $uuid */
            $uuid = $entity->getId();

            return new UuidV6($uuid->toRfc4122());
        }

        return new UuidV6();
    }
}
