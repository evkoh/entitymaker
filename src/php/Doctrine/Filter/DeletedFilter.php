<?php

declare(strict_types = 1);

namespace App\Doctrine\Filter;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Class DeletedFilter
 * @package App\Doctrine\Filter
 */
class DeletedFilter extends SQLFilter
{

    /**
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if ($targetEntity->hasField('deletedDateTime')) {
            return $targetTableAlias . '.deleted_at IS NULL';
        }

        return '';
    }

}
