<?php

declare(strict_types = 1);

namespace App\Entity\Interfaces;

use DateTimeImmutable;

/**
 * Interface EntityHistoryInterface
 * @package App\Entity\Interfaces
 */
interface EntityHistoryInterface
{

    /** @return DateTimeImmutable|null */
    public function getCreationDateTime(): ?DateTimeImmutable;

    /**
     * @param DateTimeImmutable $creationDateTime
     * @return $this
     */
    public function setCreationDateTime(DateTimeImmutable $creationDateTime); // todo php 7.4 : return self in this interface

    /** @return DateTimeImmutable */
    public function getUpdatedDateTime(): ?DateTimeImmutable;

    /**
     * @param DateTimeImmutable $updatedDateTime
     * @return EntityHistoryInterface
     */
    public function setUpdatedDateTime(DateTimeImmutable $updatedDateTime); // todo php 7.4 : return self in this interface

    /** @return DateTimeImmutable */
    public function getDeletedDateTime(): ?DateTimeImmutable;

    /**
     * @param DateTimeImmutable|null $deletedDateTime
     * @return EntityHistoryInterface
     */
    public function setDeletedDateTime(?DateTimeImmutable $deletedDateTime); // todo php 7.4 : return self in this interface

    /** @return bool */
    public function isUpdated(): bool;

    /** @return bool */
    public function isDeleted(): bool;

    /** @return $this */
    public function historyRemove(); // todo php 7.4 : return self in this interface

    /** @return array */
    public function _historyToArray(): array;

}
