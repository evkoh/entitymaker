<?php

declare(strict_types = 1);

namespace App\Entity\Interfaces;

/**
 * Interface EntityInterface
 * @package App\Entity\Interfaces
 */
interface EntityInterface
{

    /**
     * @param array $toDisqualify
     * @return array
     */
    public function __toArray(array $toDisqualify = []): array;

    /**
     * @param array $array
     * @param array $toDisqualify
     * @return array
     */
    public function disqualifier(array $array = [], array $toDisqualify = []): array;

}
