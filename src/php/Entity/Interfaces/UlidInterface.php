<?php

declare(strict_types = 1);

namespace App\Entity\Interfaces;

use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Ulid;

/**
 * Interface UlidInterface
 * @package App\Entity\Interfaces
 */
interface UlidInterface
{

    /** @return AbstractUid */
    public function getId(): AbstractUid;

    /**
     * @param Ulid $id
     * @return $this
     */
    public function setId(Ulid $id); // todo php 7.4 : return self in this interface

}
