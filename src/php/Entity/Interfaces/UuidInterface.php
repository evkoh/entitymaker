<?php

declare(strict_types = 1);

namespace App\Entity\Interfaces;

use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Uuid;

/**
 * Interface UuidInterface
 * @package App\Entity\Interfaces
 */
interface UuidInterface
{

    /** @return AbstractUid */
    public function getId(): AbstractUid;

    /**
     * @param Uuid $id
     * @return $this
     */
    public function setId(Uuid $id); // todo php 7.4 : return self in this interface

}
