<?php

declare(strict_types = 1);

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use App\ApiPlatform\Filter\Doctrine\Orm\UuidBinaryFilter;
use App\Doctrine\IdGenerator\UuidV6Generator;

/**
 * Trait UuidBinaryTrait
 * @package App\Entity\Traits
 */
trait UuidBinaryTrait
{

    // todo https://gitlab.com/evkoh/entitymaker/-/issues/23

    /**
     * @var \Symfony\Component\Uid\Uuid
     * @ApiProperty(
     *     identifier=true,
     *     description="Uuid",
     *     required=true,
     *     iri="http://schema.org/identifier"
     * )
     * @Groups({
     *     "uuid",
     *     "id"
     * })
     * @ORM\Id
     * @ORM\Column(
     *     type="uuid",
     *     unique=true,
     *     options={"comment":"Uuid"}
     *     )
     * @ApiFilter(
     *     UuidBinaryFilter::class,
     *     strategy="exact"
     * )
     * @Assert\Uuid
     * @ORM\GeneratedValue(
     *     strategy="CUSTOM"
     * )
     * @ORM\CustomIdGenerator(
     *     class=UuidV6Generator::class
     * )
     */
    protected $id;

    /** @return \Symfony\Component\Uid\Uuid */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     * @return $this
     */
    public function setId(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

}
