<?php

declare(strict_types = 1);

namespace App\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use function assert;
use App\Entity\MediaObject;

/**
 * Trait MediaObjectTrait
 * @package App\Entity\Traits
 */
trait MediaObjectTrait
{

    /** @return array<MediaObject>|Collection */
    public function getMediaObjects(): Collection
    {
        return $this->mediaObjects;
    }

    /**
     * @param array<MediaObject>|ArrayCollection $mediaObjects
     * @return $this
     */
    public function setMediaObjects(ArrayCollection $mediaObjects): self
    {
        $this->mediaObjects = $mediaObjects;

        return $this;
    }

    /**
     * @param MediaObject $mediaObject
     * @return $this
     */
    public function addMediaObject(MediaObject $mediaObject): self
    {
        if (!$this->mediaObjects->contains($mediaObject)) {
            $this->mediaObjects[] = $mediaObject;
        }

        return $this;
    }

    /**
     * @param MediaObject $mediaObject
     * @return $this
     */
    public function removeMediaObject(MediaObject $mediaObject): self
    {
        if ($this->mediaObjects->contains($mediaObject)) {
            $this->mediaObjects->removeElement($mediaObject);
        }

        return $this;
    }

    /**
     * @param string $namespace
     * @param MediaObject $mediaObject
     * @return $this
     */
    public function replaceMediaObjectByNamespace(string $namespace, MediaObject $mediaObject): self
    {
        $found = false;

        foreach ($this->mediaObjects as $mediaObjectIteration) {
            assert($mediaObjectIteration instanceof MediaObject);

            if ($mediaObjectIteration->getNamespace() !== $namespace) {
                continue;
            }

            $mediaObjectIteration->setRelative($mediaObject->getRelative());
            $found = true;
        }

        if (!$found) {
            $mediaObject->setNamespace($namespace);
            $this->addMediaObject($mediaObject);

            dump('Not found !! ' . $namespace);
        }

        return $this;
    }

    /**
     * @param string $namespace
     * @return $this
     */
    public function removeMediaObjectByNamespace(string $namespace): self
    {
        foreach ($this->mediaObjects as $mediaObjectIteration) {
            assert($mediaObjectIteration instanceof MediaObject);

            if ($mediaObjectIteration->getNamespace() !== $namespace) {
                continue;
            }

            $this->removeMediaObject($mediaObjectIteration);
        }

        return $this;
    }

}
