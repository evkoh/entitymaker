<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use App\ApiPlatform\Filter\Doctrine\Orm\UuidFilter;
use App\Doctrine\IdGenerator\UuidV6Generator;
use App\Exception\Api\InvalidObjectException;
use function is_string;

/**
 * Trait UuidV6Trait
 * @package App\Entity\Traits
 */
trait UuidV6Trait
{

    /**
     * @var int
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @ORM\Column(
     *     name="id",
     *     type="integer",
     *     nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(
     *     strategy="AUTO"
     * )
     */
    private $id;

    /**
     * @var string
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @ApiProperty(
     *     identifier=true,
     *     description="Uuid",
     *     required=true,
     *     iri="http://schema.org/identifier"
     * )
     * @ApiFilter(
     *     UuidFilter::class,
     *     strategy="exact"
     * )
     * @Groups("uuid")
     * @ORM\Column(
     *     type="string",
     *     length=36,
     *     unique=true,
     *     options={"comment":"Uuid"}
     * )
     * @Assert\Uuid
     * @ORM\GeneratedValue(
     *     strategy="CUSTOM"
     * )
     * @ORM\CustomIdGenerator(
     *     class=UuidV6Generator::class
     * )
     */
    protected $uuid;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \Symfony\Component\Uid\AbstractUid
     * @throws InvalidObjectException
     */
    public function getUuid(): AbstractUid
    {
        if (is_string($this->uuid)) {
            return Uuid::fromString($this->uuid);
        }

        if ($this->uuid instanceof Uuid) {
            return $this->uuid;
        }

        if (null === $this->uuid) {
            return $this->setUuid(Uuid::v6())->getUuid();
        }

        throw new InvalidObjectException('The id is not string or Uuid instance !');
    }

    /**
     * @param Uuid $uuid
     * @return $this
     */
    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid->toRfc4122();

        return $this;
    }

    /**
     *
     */
    protected function resetUuidV6(): void
    {
        $this->uuid = null;
    }

}
