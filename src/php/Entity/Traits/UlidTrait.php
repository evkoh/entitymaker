<?php

declare(strict_types = 1);

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Validator\Constraints as Assert;
use App\ApiPlatform\Filter\Doctrine\IdGenerator\UlidGenerator;
use App\ApiPlatform\Filter\Doctrine\Orm\UlidFilter;
use App\Exception\Api\InvalidObjectException;

/**
 * Trait UlidTrait
 * @package App\Entity\Traits
 */
trait UlidTrait
{
    // todo https://gitlab.com/evkoh/entitymaker/-/issues/23

    /**
     * @var string
     * @ApiProperty(
     *     identifier=true,
     *     description="Ulid",
     *     required=true,
     *     iri="http://schema.org/identifier"
     * )
     * @Groups({
     *     "uuid",
     *     "id"
     * })
     * @ORM\Id
     * @ORM\Column(
     *     type="string",
     *     unique=true,
     *     options={"comment":"Ulid"}
     * )
     * @ApiFilter(
     *     UlidFilter::class,
     *     strategy="exact"
     * )
     * @Assert\Ulid
     * @ORM\GeneratedValue(
     *     strategy="CUSTOM"
     * )
     * @ORM\CustomIdGenerator(
     *     class=UlidGenerator::class
     * )
     */
    private $id;

    /**
     * @throws InvalidObjectException
     *
     * @return \Symfony\Component\Uid\AbstractUid
     */
    public function getId(): AbstractUid
    {
        if (\is_string($this->id)) {
            return Ulid::fromString($this->id);
        }

        if ($this->id instanceof Ulid) {
            return $this->id;
        }

        if (null === $this->id) {
            return $this->setId(new Ulid())->getId();
        }

        throw new InvalidObjectException('The id is not string or Ulid instance !');
    }

    /**
     * @param Ulid $id
     *
     * @return $this
     */
    public function setId(Ulid $id): self
    {
        $this->id = $id->toRfc4122();

        return $this;
    }
}
