<?php

declare(strict_types = 1);

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\ApiPlatform\Filter\Doctrine\Orm\ExtensibleFilter;

/**
 * Trait HistoryTrait
 * @package App\Entity\Traits
 */
trait HistoryTrait
{
    /**
     * @var DateTimeImmutable
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="date-time"}
     *     }
     * )
     * @Groups(
     *     "history"
     * )
     * @ORM\Column(
     *     name="created_at",
     *     nullable=true,
     *     type="datetime_immutable",
     *     options={"comment":"Date de création"}
     * )
     * @ApiFilter(
     *     DateFilter::class,
     *     strategy="partial"
     * )
     * @ApiFilter(
     *     ExtensibleFilter::class,
     *     strategy="exact"
     * )
     * todo : I put nullable = true, but it shouldn't! This is required for the parameter to be optional in the GraphQL query
     */
    protected $creationDateTime;

    /**
     * @var DateTimeImmutable
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="date-time"}
     *     }
     * )
     * @Groups("history")
     * @ORM\Column(name="updated_at", nullable=true, type="datetime_immutable", options={"comment":"Date de modification"})
     * @ApiFilter(DateFilter::class, strategy="partial")
     * @ApiFilter(ExtensibleFilter::class, strategy="exact")
     * todo : I put nullable = true, but it shouldn't! This is required for the parameter to be optional in the GraphQL query
     */
    protected $updatedDateTime;

    /**
     * @var DateTimeImmutable|null
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="date-time"}
     *     }
     * )
     * @Groups("history")
     * @ORM\Column(name="deleted_at", type="datetime_immutable", nullable=true, options={"comment":"Date de suppression"})
     * @ApiFilter(DateFilter::class, strategy="partial")
     * @ApiFilter(ExtensibleFilter::class, strategy="exact")
     */
    protected $deletedDateTime;

    /** @return DateTimeImmutable|null */
    public function getCreationDateTime(): ?DateTimeImmutable
    {
        return $this->creationDateTime;
    }

    /**
     * @param DateTimeImmutable $creationDateTime
     *
     * @return $this
     */
    public function setCreationDateTime(DateTimeImmutable $creationDateTime): self
    {
        $this->creationDateTime = $creationDateTime;

        return $this;
    }

    /** @return DateTimeImmutable|null */
    public function getUpdatedDateTime(): ?DateTimeImmutable
    {
        return $this->updatedDateTime;
    }

    /**
     * @param DateTimeImmutable $updatedDateTime
     *
     * @return $this
     */
    public function setUpdatedDateTime(DateTimeImmutable $updatedDateTime): self
    {
        $this->updatedDateTime = $updatedDateTime;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUpdated(): bool
    {
        return $this->getUpdatedDateTime() !== $this->getCreationDateTime();
    }

    /** @return DateTimeImmutable|null */
    public function getDeletedDateTime(): ?DateTimeImmutable
    {
        return $this->deletedDateTime;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return null === $this->getDeletedDateTime();
    }

    /**
     * @param DateTimeImmutable|null $deletedDateTime
     *
     * @return $this
     */
    public function setDeletedDateTime(?DateTimeImmutable $deletedDateTime): self
    {
        $this->deletedDateTime = $deletedDateTime;

        return $this;
    }

    private function historyInit(): void
    {
        $now = new DateTimeImmutable();

        $this->setCreationDateTime($now);
        $this->setUpdatedDateTime($now);
        $this->setDeletedDateTime(null);
    }

    /**
     * @return $this
     * @ORM\PrePersist()
     */
    private function historyUpdate(): self
    {
        $this->setUpdatedDateTime(new DateTimeImmutable());

        return $this;
    }

    /**
     * @return $this
     */
    public function historyRemove(): self
    {
        $this->setDeletedDateTime(new DateTimeImmutable());

        return $this;
    }

    protected function resetHistory(): void
    {
        $this->creationDateTime = null;
        $this->updatedDateTime  = null;
        $this->deletedDateTime  = null;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function prePersist(): void
    {
        $this->historyUpdate();

        if (null !== $this->getCreationDateTime()) {
            return;
        }

        $this->historyInit();
    }

    /** @return array */
    public function _historyToArray(): array
    {
        return [
            'creationDate' => $this->getCreationDateTime(),
            'updatedDate' => $this->getUpdatedDateTime(),
            'deletedDate' => $this->getDeletedDateTime(),
        ];
    }
}
