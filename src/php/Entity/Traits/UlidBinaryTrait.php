<?php

declare(strict_types = 1);

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Validator\Constraints as Assert;
use App\ApiPlatform\Filter\Doctrine\IdGenerator\UlidGenerator;
use App\ApiPlatform\Filter\Doctrine\Orm\UlidBinaryFilter;

/**
 * Trait UlidBinaryTrait
 * @package App\Entity\Traits
 */
trait UlidBinaryTrait
{
    // todo https://gitlab.com/evkoh/entitymaker/-/issues/23

    /**
     * @var \Symfony\Component\Uid\Ulid
     * @ApiProperty(identifier=true, description="Ulid", required=true, iri="http://schema.org/identifier")
     * @Groups({
     *     "uuid",
     *     "id"
     * })
     * @ORM\Id
     * @ORM\Column(type="ulid", unique=true, options={"comment":"Ulid"})
     * @ApiFilter(UlidBinaryFilter::class, strategy="exact")
     * @Assert\Ulid
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    private $id;

    /** @return \Symfony\Component\Uid\Ulid */
    public function getId(): Ulid
    {
        return $this->id;
    }

    /**
     * @param Ulid $id
     *
     * @return $this
     */
    public function setId(Ulid $id): self
    {
        $this->id = $id;

        return $this;
    }
}
