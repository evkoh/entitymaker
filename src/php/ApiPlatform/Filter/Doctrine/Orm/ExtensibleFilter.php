<?php

declare(strict_types = 1);

namespace App\ApiPlatform\Filter\Doctrine\Orm;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use function explode;
use function mb_strpos;
use function sprintf;
use function trim;

/**
 * Class ExtensibleFilter
 * @package App\ApiPlatform\Filter\Doctrine\Orm
 */
class ExtensibleFilter extends AbstractContextAwareFilter
{

    /**
     * This function is only used to hook in documentation generators (supported by Swagger and Hydra)
     *
     * @param string $resourceClass
     * @return array
     */
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];

        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => 'array',
                'required' => false,
                'swagger' => [
                    'description' => 'Not in array values',
                    'name' => 'id',
                    'type' => 'array',
                ],
            ];
        }

        return $description;
    }

    /**
     * @param string $property
     * @param $values
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param string|null $operationName
     */
    protected function filterProperty(
        string $property,
        $values,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?string $operationName = null
    ): void {
        if (
            !$this->isPropertyEnabled($property, $resourceClass)
            || !$this->isPropertyMapped($property, $resourceClass)
        ) {
            return;
        }

        $and = $queryBuilder->expr()->andx();

        foreach ($values as $value) {
            $parameterName = $queryNameGenerator->generateParameterName($property);

            $value = trim($value);

            // null / not null

            if ('!=null' === $value || '!null' === $value || 'is not null' === $value) {
                $and->add(sprintf('o.%s IS NOT NULL', $property));

                continue;
            }

            if ('=null' === $value || '==null' === $value || '===null' === $value || 'is null' === $value) {
                $and->add(sprintf('o.%s IS NULL', $property));

                continue;
            }

            // empty / not empty

            if ('!=empty' === $value || '!empty' === $value || 'is not empty' === $value) {
                $and->add(sprintf('size(o.%s) > 0', $property));

                continue;
            }

            if ('=empty' === $value || '==empty' === $value || '===empty' === $value || 'is empty' === $value) {
                $and->add(sprintf('size(o.%s) <= 0', $property));

                continue;
            }

            // equal / not equal

            if (false !== mb_strpos($value, '!=')) {
                $and->add(sprintf('o.%s != :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('!=', $value)[0])));

                continue;
            }

            if (false !== mb_strpos($value, '==')) {
                $and->add(sprintf('o.%s = :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('==', $value)[0])));

                continue;
            }

            // gt / gte

            if (false !== mb_strpos($value, 'gt:')) {
                $and->add(sprintf('o.%s > :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('gt:', $value)[0])));

                continue;
            }

            if (false !== mb_strpos($value, 'gte:')) {
                $and->add(sprintf('o.%s >= :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('gte:', $value)[0])));

                continue;
            }

            // lt / lte

            if (false !== mb_strpos($value, 'lt:')) {
                $and->add(sprintf('o.%s < :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('lt:', $value)[0])));

                continue;
            }

            if (false !== mb_strpos($value, 'lte:')) {
                $and->add(sprintf('o.%s <= :%s', $property, $parameterName));
                $queryBuilder->getParameters()->add(new Parameter($parameterName, trim(explode('lte:', $value)[0])));

                continue;
            }
        }

        $queryBuilder->andWhere($and);
    }

}
