<?php

declare(strict_types = 1);

namespace App\ApiPlatform\Filter\Doctrine\Orm;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use function sprintf;

/**
 * Class UlidFilter
 * @package App\ApiPlatform\Filter\Doctrine\Orm
 */
class UlidFilter extends AbstractContextAwareFilter
{

    /**
     * This function is only used to hook in documentation generators (supported by Swagger and Hydra)
     *
     * @param string $resourceClass
     * @return array
     */
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];

        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => 'array',
                'required' => false,
                'swagger' => [
                    'description' => 'Many ULID in array',
                    'name' => 'id',
                    'type' => 'array',
                ],
            ];
        }

        return $description;
    }

    /**
     * @param string $property
     * @param $values
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param string|null $operationName
     */
    protected function filterProperty(
        string $property,
        $values,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?string $operationName = null
    ): void {
        if (
            !$this->isPropertyEnabled($property, $resourceClass)
            || !$this->isPropertyMapped($property, $resourceClass)
        ) {
            return;
        }

        $or = $queryBuilder->expr()->orx();

        foreach ($values as $ulidString) {
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $or->add(sprintf('o.%s = :%s', $property, $parameterName));
            $queryBuilder->getParameters()->add(new Parameter($parameterName, $ulidString, 'string'));
        }

        $queryBuilder->andWhere($or);
    }

}
