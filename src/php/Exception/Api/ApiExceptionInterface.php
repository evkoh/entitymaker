<?php

declare(strict_types = 1);

namespace App\Exception\Api;

/**
 * Interface ApiExceptionInterface
 * @package App\Exception\Api
 */
interface ApiExceptionInterface
{

    public function getDetails(): ?array;

    /**
     * @param string $key
     * @param $details
     * @return $this
     */
    public function addDetail(string $key, $details); // todo php 7.4 : return self in this interface

}
