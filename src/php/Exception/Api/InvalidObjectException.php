<?php

declare(strict_types = 1);

namespace App\Exception\Api;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class InvalidObjectException
 * @package App\Exception\Api
 */
class InvalidObjectException extends AbstractApiException
{

    /**
     * InvalidObjectException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, $code = Response::HTTP_BAD_REQUEST, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
