<?php

declare(strict_types=1);

namespace App\Exception\Api;

use Exception;

/**
 * Class AbstractApiException
 * @package App\Exception\Api
 */
abstract class AbstractApiException extends Exception implements ApiExceptionInterface
{

    /**
     * @var array
     */
    private $details = [];

    /** @return array|null */
    public function getDetails(): ?array
    {
        return $this->details;
    }

    /**
     * @param string $key
     * @param $details
     * @return $this
     */
    public function addDetail(string $key, $details): self
    {
        $this->details[$key] = $details;

        return $this;
    }

}
