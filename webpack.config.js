const webpack = require("webpack");
const path = require("path");
let OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// let UglifyJSPlugin = require('uglifyjs-webpack-plugin')
// frontoffice.outilmaquettage.local

// https://varya.me/blog/webfonts-with-sass-and-webpack/

// module.exports = {
//     mode: 'development',
//     entry: './app/app.js',
//     output: {
//         path: path.resolve(__dirname, 'public/dist'),
//         filename: 'app.js'
//     },
//     devServer: {
//         host: '0.0.0.0',
//         disableHostCheck: true,
//         contentBase: path.join(__dirname, 'public'),
//         watchContentBase: true,
//         compress: true,
//         port: 80,
//         hot: true
//     }
// };

let config = {
    mode: 'development',
    resolve: {
        // alias: {
        //     'jquery-ui': path.resolve(__dirname, 'node_modules/jquery-ui/ui')
        // },
        fallback: {
            fs: false,
            "crypto": require.resolve("crypto-browserify"),
            "stream": require.resolve("stream-browserify")
        },
        extensions: ['.tsx', '.ts', '.js'],
    },
    entry: {
        "app": "./src/entry/app.js",
    },
    output: {
        path: path.resolve(__dirname, "./public"),
        filename: 'dist/js/[name].js'
    },
    //devtool:'source-map',
    plugins: [
        new webpack.ProvidePlugin({
            process: 'process/browser',
            jQuery: "jquery",
            jquery: "jquery",
            $: "jquery",
            "window.jQuery": "jquery",
            "window.jquery": "jquery",
            "window.$": "jquery"
        }),
        // new UglifyJSPlugin(),
        new MiniCssExtractPlugin(),
        // new BrowserSyncPlugin(
        //     // BrowserSync options
        //     {
        //         host: '0.0.0.0',
        //         port: 80
        //     },
        //     // plugin options
        //     {
        //         // prevent BrowserSync from reloading the page
        //         // and let Webpack Dev Server take care of this
        //         reload: false
        //     }
        // )
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Resolve URLs in SASS
                    "resolve-url-loader",
                    // Compiles Sass to CSS
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                outputStyle: "compressed",
                            },
                        },
                    }
                ],
                // exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ],
                // exclude: /node_modules/,
            },
            {
                test: /\.(svg|eot|woff|woff2|ttf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'font/'
                        }
                    }
                ],
                // exclude: /node_modules/,
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/'
                        }
                    }
                ],
                // exclude: /node_modules/,
            },
            {
                test: /\.json$/,
                use: ['json-loader'],
                type: 'javascript/auto'
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    // Remove comments from JS files
    optimization: {
        minimizer: [
            // new UglifyJsPlugin({
            //     uglifyOptions: {
            //         output: {
            //             comments: false,
            //         },
            //     },
            // }),
            new OptimizeCSSAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: [
                        'default', {
                            discardComments: {
                                removeAll: true
                            }
                        }
                    ],
                }
            })
        ],
    },
    devServer: {
        host: '0.0.0.0',
        disableHostCheck: true,
        contentBase: path.join(__dirname, 'public'),
        watchContentBase: true,
        watchOptions: {
            ignored: [
                path.resolve(__dirname, 'dist'),
                path.resolve(__dirname, 'node_modules')
            ]
        },
        compress: true,
        port: 4200,
        hot: true,
        historyApiFallback: {
            index: 'index.html'
        }
    },
};

module.exports = config;